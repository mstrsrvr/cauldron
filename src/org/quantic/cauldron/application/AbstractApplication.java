package org.quantic.cauldron.application;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.quantic.cauldron.application.target.EqualsTarget;
import org.quantic.cauldron.application.target.Target;
import org.quantic.cauldron.datamodel.DataContext;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.datamodel.Query;
import org.quantic.cauldron.datamodel.TypeContext;
import org.quantic.cauldron.datamodel.TypeContextEntry;
import org.quantic.cauldron.facilities.CauldronFacility;
import org.quantic.cauldron.facilities.pageactions.PageContextGet;
import org.quantic.cauldron.facility.AbstractFacility;
import org.quantic.cauldron.facility.Facility;
import org.quantic.cauldron.facility.Function;
import org.quantic.cauldron.facility.FunctionBySession;
import org.quantic.cauldron.facility.FunctionConfig;
import org.quantic.cauldron.facility.FunctionExecutable;
import org.quantic.cauldron.facility.FunctionModule;
import org.quantic.cauldron.output.ButtonFactory;
import org.quantic.cauldron.output.CheckBoxFactory;
import org.quantic.cauldron.output.DataFormFactory;
import org.quantic.cauldron.output.FileUploadFactory;
import org.quantic.cauldron.output.GenericFactory;
import org.quantic.cauldron.output.LinkFactory;
import org.quantic.cauldron.output.ListBoxFactory;
import org.quantic.cauldron.output.MessageFactory;
import org.quantic.cauldron.output.TabbedPaneFactory;
import org.quantic.cauldron.output.TableFactory;
import org.quantic.cauldron.output.TextFactory;
import org.quantic.cauldron.output.TextFieldFactory;
import org.quantic.cauldron.output.ToolFactory;
import org.quantic.cauldron.output.TreeToolFactory;
import org.quantic.cauldron.pagectx.PageContext;
import org.quantic.cauldron.pagectx.PageSpec;
import org.quantic.cauldron.pagectx.PageSpecItem;
import org.quantic.cauldron.pagectx.CauldronPage;
import org.quantic.cauldron.pagectx.CauldronPageType;
import org.quantic.cauldron.pagectx.PageConfig;
import org.quantic.cauldron.pagectx.PageSpecItem.TYPES;
import org.quantic.cauldron.pagectx.StandardPages;
import org.quantic.cauldron.trace.CauldronExecutionData;
import org.quantic.cauldron.trace.CauldronProgram;
import org.quantic.cauldron.trace.SignalData;

public abstract class AbstractApplication
        extends AbstractFacility implements Application {
    private SharedContext sharedctx;
    private Map<TYPES, ToolFactory> htmlfacs;
    private Connector connector;
    private Map<String, Target> targets;
    private List<FunctionBySession> fncsession;
    private CauldronRenderer renderer;
    private Map<CauldronPageType, PageType> pagetypes;
    private TypeContextEntry numobjtype;
    private ApplicationInit appinit;
    
    public AbstractApplication(
            ApplicationContext context, SharedContext sharedctx, String name) {
        super(context, name);
        TypeContext typectx;
        TypeContextEntry type;
        this.sharedctx = sharedctx;
        this.sharedctx.add(this);
        
        targets = new HashMap<>();
        fncsession = new ArrayList<>();
        htmlfacs = new HashMap<>();
        htmlfacs.put(TYPES.BUTTON, new ButtonFactory());
        htmlfacs.put(TYPES.CHECK_BOX, new CheckBoxFactory());
        htmlfacs.put(TYPES.DATA_FORM, new DataFormFactory());
        htmlfacs.put(TYPES.FILE_UPLOAD, new FileUploadFactory());
        htmlfacs.put(TYPES.LINK, new LinkFactory());
        htmlfacs.put(TYPES.LISTBOX, new ListBoxFactory());
        htmlfacs.put(TYPES.MESSAGE, new MessageFactory());
        htmlfacs.put(TYPES.NODE_LIST, new GenericFactory("ul"));
        htmlfacs.put(TYPES.NODE_LIST_ITEM, new GenericFactory("li"));
        htmlfacs.put(TYPES.STANDARD_CONTAINER, new GenericFactory("div"));
        htmlfacs.put(TYPES.TABBED_PANE, new TabbedPaneFactory());
        htmlfacs.put(TYPES.TABLE_TOOL, new TableFactory());
        htmlfacs.put(TYPES.TEXT, new TextFactory());
        htmlfacs.put(TYPES.TEXT_FIELD, new TextFieldFactory());
        htmlfacs.put(TYPES.TREE_TOOL, new TreeToolFactory());
        
        pagetypes = new HashMap<>();
        pagetypes.put(CauldronPageType.BASE, (p, n)->p.base(n));
        pagetypes.put(CauldronPageType.PANEL, (p, n)->p.instance(n));
        
        typectx = context.typectx();
        numobjtype = typectx.define("number_object", "type");
        numobjtype.addst("type");
        numobjtype.addl("current");
        
        type = typectx.define("display_document");
        type.addst("page");
        type.addst("facility");

        type = typectx.define("page_context");
        type.addst("action");

        type = typectx.define("page_address");
        type.addst("page");
        type.add(typectx.get("any"), "output");

        type = typectx.define("page_context");
        context.datactx().instance(type, "pagectx");
        
        target("page_call", name.concat(".page_context"));

        add(()->new PageContextGet(name));
        add(()->newpageoutput());
    }
    
    public AbstractApplication(SharedContext sharedctx, String name) {
        this(new ApplicationContext(sharedctx, name), sharedctx, name);
    }

    @Override
    public final void add(FunctionBySession function) {
        fncsession.add(function);
    }
    
    private final PageContext compile(Context context) {
        PageSpec spec;
        PageConfig config;
        String name = context.getPage();
        ApplicationContext appctx = getContext();
        CauldronPage cpage = appctx.pages.get(name);
        
        if (cpage.pagectx == null) {
            cpage.pagectx = pagetypes.get(cpage.type).
                    instance(new StandardPages(appctx), name);
            cpage.pagectx.set(cpage);
        }
        
        spec = cpage.pagectx.getSpec();
        spec.run(cpage.pagectx);
        for (PageSpecItem specitem : spec.getItems())
            cpage.pagectx.instance(specitem);
        
        if ((config = cpage.pagectx.getConfig()) != null)
            config.run(cpage.pagectx, context);
        return cpage.pagectx;
    }
    
    @Override
    public final Context context() {
        return context(sharedctx);
    }
    
    private final Context context(SharedContext sharedctx) {
        return new CauldronContext(sharedctx, getName());
    }
    
    protected final ContextEntry ctxentry() {
        return new ContextEntry(context());
    }
    
    protected void dbinit(Context context) {
        Set<String> documents = connector.getDocuments();
        TypeContext typectx = context.typectx();
        for (String key : typectx.getTypes())
            if (!documents.contains(key))
                connector.create(typectx.get(key));
    }
    
    private final void execute(ContextEntry ctxentry) throws Exception {
        TypeContextEntry type;
        DataContextEntry output;
        CauldronProgram program;
        CauldronExecutionData execdata;
        Target target = targets.get(ctxentry.action);
        
        if (!target.deptest(ctxentry)) {
            ctxentry.targetstack.push(ctxentry.action);
            ctxentry.action = target.context().requires;
            throw new TargetRequirementException();
        }
        
        /*
         * get a program according action (in cache or bulding it runtime
         */
        program = getProgram(ctxentry, null, ctxentry.action);
        if (program == null)
            throw new CauldronException(
                    "failed building program for %s.", ctxentry.action);
        
        /*
         * initializing session factory 
         */
        if (ctxentry.sessionfac == null)
            ctxentry.sessionfac = facilitiesinit(
                    this, ctxentry.context, ctxentry.sessionid);
        
        /*
         * execute program
         */
        execdata = new CauldronExecutionData();
        output = program.execute(ctxentry.context, execdata);
        if (execdata.signal != null) {
            executeSignalProgram(ctxentry, execdata);
            ctxentry.onsignal = true;
            return;
        }
        
        /*
         * save result
         */
        ctxentry.onsignal = false;
        ctxentry.targetsok.add(ctxentry.action);
        if ((output == null) || ((type = output.getType()).getKey() == null))
            return;
        if (type.isAutoGenKey())
            generateKey(ctxentry, output);
        ctxentry.dbsession.save(output);
    }
    
    private final void executeSignalProgram(
            ContextEntry ctxentry, CauldronExecutionData execdata)
                    throws Exception {
        CauldronProgram program = getProgram(ctxentry, execdata.signal, null);
        if (program == null)
            throw new CauldronException(
                    "failed building program for %s.", ctxentry.action);
        execdata.signal.get(ctxentry.context);
        program.execute(ctxentry.context, execdata);
    }
    
    protected final Facility facilitiesinit(
            Application application, Context context, String sessionid) {
        String sfacname = new StringBuilder("session_fac_").
                append(sessionid).toString();
        Facility facility = application.facility(context, sfacname);
        for (FunctionBySession function : fncsession)
            facility.add(function.instance());
        return facility;
    }
    
    @Override
    public final Facility facility(String name) {
        return facility(new CauldronContext(sharedctx, name), name);
    }
    
    @Override
    public final Facility facility(Context context, String name) {
        Facility facility = new CauldronFacility(context, name);
        sharedctx.add(facility);
        return facility;
    }
    
    private final String getAbsoluteType(String facility, String type) {
        return new StringBuilder(facility).append(".").append(type).toString();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public final <C extends Context> C getContext(String facility) {
        return (C)sharedctx.get(facility).getContext();
    }
    
    protected final ToolFactory getFactory(TYPES type) {
        return htmlfacs.get(type);
    }
    
    private final void generateKey(
            ContextEntry ctxentry, DataContextEntry data) {
        Query query;
        TypeContextEntry type = data.getType();
        String objname = type.getFullName();
        DataContextEntry numobj = ctxentry.dbsession.get(numobjtype, objname);
        long id = (numobj == null)? 1 : numobj.getl("current") + 1;
        
        if (id == 1) {
            numobj = ctxentry.context.datactx().instance(numobjtype, "numobj");
            numobj.set("type", objname);
            numobj.set("current", id);
            ctxentry.dbsession.insert(numobj);
        } else {
            query = new Query();
            query.values("current", id);
            query.setType(numobj.getType());
            query.andEqual("type", objname);
            ctxentry.dbsession.update(query);
        }
        data.set(type.getKey(), id);
    }
    
    private final CauldronProgram getProgram(ContextEntry ctxentry,
            SignalData signal, String action) throws Exception {
        List<CauldronProgram> success;
        Target target;
        BuildApplication buildapp;
        
        /*
         * program validation
         */
        if (signal != null)
            action = signal.name;
        
        if (ctxentry.programs.containsKey(action))
            return ctxentry.programs.get(action);

        target = targets.get(action);
        if (!this.sharedctx.types.containsKey(target.context().returns))
            throw new CauldronException("target '%s' not found.", action);

        
        buildapp = ctxentry.buildapp(getName());
        success = buildapp.execute(target, signal, appinit);
        for (CauldronProgram program : success) {
            ctxentry.programs.put(action, program);
            return program;
        }
        return null;
    }
    
    @Override
    public final void init(ApplicationInit appinit) {
        ApplicationContext context = getContext();
        (this.appinit = appinit).run(this);
        screengetinit(context);
        dbinit(context);
    }
    
    private final Function newpageoutput() {
        return new FunctionModule("page_output", (c)->pageoutput(c));
    }
    
    @Override
    public final CauldronPage page(String name) {
        CauldronPage cpage = new CauldronPage();
        ((ApplicationContext)getContext()).pages.put(name, cpage);
        return cpage;
    }
    
    private final FunctionExecutable pageoutput(FunctionConfig config) {
        String name = getName();
        config.input(name.concat(".page_address"), "signal");
        config.output(name.concat(".page_context"));
        return (s)-> {
            s.context().setPage(s.geto("signal").getst("page"));
        };
    }
    
    protected final void put(TYPES type, ToolFactory factory) {
        htmlfacs.put(type, factory);
    }
    
    protected final void rmfacility(String name) {
        sharedctx.remove(name);
    }

    private final void rollback(ContextEntry ctxentry) {
        if (ctxentry.dbsession != null)
            ctxentry.dbsession.rollback();
        ctxentry.targetsok.remove(ctxentry.action);
        ctxentry.onsignal = false;
        ctxentry.targetcontinue = false;
    }
    
    @Override
    public String run(ContextEntry ctxentry) throws Exception {
        String output;
        try {
            if (ctxentry.dbsession == null)
                ctxentry.dbsession = connector.instance(ctxentry.context);
            if (!ctxentry.targetcontinue)
                ctxentry.dbsession.start();
            ctxentry.pageready = !(ctxentry.pagestack.size() == 0);
            execute(ctxentry);
            if (!ctxentry.onsignal && (ctxentry.targetstack.size() > 0)) {
                ctxentry.action = ctxentry.targetstack.pop();
                ctxentry.targetcontinue = true;
                return run(ctxentry);
            }
            ctxentry.targetcontinue = false;
            ctxentry.context.reset();
            if (!ctxentry.pageready) {
                ctxentry.pagectx = compile(ctxentry.context);
                ctxentry.pageready = true;
            }
            output = renderer.run(ctxentry);
            ctxentry.dbsession.commit();
            return output;
        } catch (TargetRequirementException e) {
            rollback(ctxentry);
            if (ctxentry.action != null)
                return run(ctxentry);
            throw e;
        } catch (Exception e) {
            rollback(ctxentry);
            throw e;
        }
    }
    
    private final FunctionExecutable screenget(FunctionConfig config,
            ApplicationContext appctx, String page, String rettype) {
        String name = getName();
        CauldronPage cpage = appctx.pages.get(page);
        for (String key : cpage.input.keySet())
            config.input(cpage.input.get(key), key);
        config.output(getAbsoluteType(name, rettype));
        return (s)->{
            SignalData pagecall;
            DataContext datactx = s.userctx().datactx();
            Set<String> entries = datactx.getReferences(rettype);
            
            if (entries == null) {
                for (String key : cpage.input.keySet())
                    datactx.set(key, s.geto(key));
                pagecall = new SignalData("page_call");
                pagecall.datatype = name.concat(".page_address");
                pagecall.data.put("page", page);
                pagecall.data.put("output", s.output());
                s.signal(pagecall);
            }
            
            for (String key : entries) {
                s.set(datactx.get(key));
                return;
            }
        };
    }
    
    private final void screengetinit(ApplicationContext appctx) {
        CauldronPage cpage;
        String fname;
        for (String key : appctx.pages.keySet()) {
            cpage = appctx.pages.get(key);
            cpage.output.add("display_document");
            for (String otype : cpage.output) {
                fname = new StringBuilder(key).append("_screen_get_").
                        append(otype).toString();
                add(new FunctionModule(
                        fname, (c)->screenget(c, appctx, key, otype)));
            }
        }
    }
    
    @Override
    public final void set(Connector connector) {
        this.connector = connector;
    }
    
    public final void set(CauldronRenderer renderer) {
        this.renderer = renderer;
    }
    
    @Override
    public final void target(String action, String document) {
        target(action, new EqualsTarget(document));
    }
    
    @Override
    public final void target(String action, Target target) {
        targets.put(action, target);
    }
    
}

interface PageType {
    
    public abstract PageContext instance(StandardPages pages, String name);
    
}

class TargetRequirementException extends Exception {
    private static final long serialVersionUID = -4031461385385342603L;
}
