package org.quantic.cauldron.application;

import java.util.HashMap;
import java.util.Map;

import org.quantic.cauldron.pagectx.CauldronPage;

public class ApplicationContext extends CauldronContext {
    public Map<String, CauldronPage> pages;
    public Map<String, ContextEntry> ctxentries;
    
    public ApplicationContext(SharedContext sharedctx, String facility) {
        super(sharedctx, facility);
        pages = new HashMap<>();
        ctxentries = new HashMap<>();
    }
    
}
