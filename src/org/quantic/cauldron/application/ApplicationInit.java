package org.quantic.cauldron.application;

public interface ApplicationInit {
    
    public abstract void run(Application application);
    
}
