package org.quantic.cauldron.application;

import java.util.Set;

import org.quantic.cauldron.datamodel.TypeContextEntry;

public interface Connector {
    
    public abstract void create(TypeContextEntry type);
    
    public abstract Set<String> getDocuments();
    
    public abstract DBSession instance(Context context);
}
