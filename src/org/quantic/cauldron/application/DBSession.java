package org.quantic.cauldron.application;

import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.datamodel.Query;
import org.quantic.cauldron.datamodel.TypeContextEntry;

public interface DBSession {
	
	public abstract void commit();
	
	public abstract DataContextEntry get(TypeContextEntry type, Object key);
	
	public abstract void insert(DataContextEntry data);
	
	public abstract void rollback();
	
	public abstract void save(DataContextEntry data);
	
	public abstract void start();
	
	public abstract void update(Query query);
	
	public abstract void update(DataContextEntry data);
	
}