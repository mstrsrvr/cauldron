package org.quantic.cauldron.application;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.quantic.cauldron.facility.Facility;

public class SharedContext {
    private Map<String, Facility> facilities;
    public Map<String, String[]> types;
    
    public SharedContext() {
        facilities = new HashMap<>();
        types = new HashMap<>();
    }
    
    public final void add(Facility facility) {
        facilities.put(facility.getName(), facility);
    }
    
    public final Facility get(String name) {
        return facilities.get(name);
    }
    
    public final Set<String> keySet() {
        return facilities.keySet();
    }
    
    public final void remove(String name) {
        facilities.remove(name);
    }
}
