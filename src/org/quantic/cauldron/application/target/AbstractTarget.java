package org.quantic.cauldron.application.target;

import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.application.TestContext;
import org.quantic.cauldron.datamodel.DataContextEntry;

public abstract class AbstractTarget implements Target {
    private TargetContext context;
    
    public AbstractTarget() {
        context = new TargetContext();
        context.rules.put("page_call", (c)->true);
        config(context);
    }
    
    protected abstract void config(TargetContext context);
    
    @Override
    public final TargetContext context() {
    	return context;
    }
    
    @Override
    public final boolean deptest(ContextEntry ctxentry) {
    	if (context.requires == null)
    		return true;
    	return ctxentry.targetsok.contains(context.requires);
    }

    @Override
    public void definputs(TestContext context) {
        DataContextEntry data;
        if (context.signal == null)
            return;
        switch (context.signal.name) {
        case "page_call":
            data = context.datactx.instance(
                    context.signal.datatype, "signal_object");
            for (String key : context.signal.data.keySet())
                data.set(key, context.signal.data.get(key));
        }
    }

    @Override
    public final boolean sigtest(TestContext context) {
        return test(context, this.context.rules.get(context.signal.name));
    }
    
    private final boolean test(TestContext context, Rule rule) {
        try {
            return rule.test(context);
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public final boolean test(TestContext context) {
        return test(context, this.context.rule);
    }
}
