package org.quantic.cauldron.application.target;

import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.application.TestContext;

public interface Target {
	
	public abstract TargetContext context();
	
	public abstract boolean deptest(ContextEntry ctxentry);
	
	public abstract void definputs(TestContext context);
	
	public abstract boolean sigtest(TestContext context);

	public abstract boolean test(TestContext context);
    
}
