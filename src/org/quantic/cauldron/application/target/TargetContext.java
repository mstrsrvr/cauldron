package org.quantic.cauldron.application.target;

import java.util.HashMap;
import java.util.Map;

public class TargetContext {
    public String returns, requires;
    public Rule rule;
    public Map<String, Rule> rules;

    public TargetContext() {
        rules = new HashMap<>();
    }
}
