package org.quantic.cauldron.application.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.quantic.cauldron.application.AbstractApplication;
import org.quantic.cauldron.application.ApplicationContext;
import org.quantic.cauldron.application.CauldronContext;
import org.quantic.cauldron.application.CauldronException;
import org.quantic.cauldron.application.CauldronRenderer;
import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.application.SharedContext;
import org.quantic.cauldron.output.ToolFactory;
import org.quantic.cauldron.output.XMLElement;
import org.quantic.cauldron.output.web.FormFactory;
import org.quantic.cauldron.pagectx.ToolData;
import org.quantic.cauldron.pagectx.Handler;
import org.quantic.cauldron.pagectx.PageSpecItem.TYPES;

public class WebApplication extends AbstractApplication {
    private PageNavigation[] navigation;
    
    public WebApplication(
            ApplicationContext context, SharedContext sharedctx, String name) {
        super(context, sharedctx, name);
    	put(TYPES.FORM, new FormFactory());
    	set((CauldronRenderer)(c)->renderer(c));
    	
    	navigation = new PageNavigation[2];
    	navigation[CauldronContext.HOME - 1] = (c)->home(c);
    	navigation[CauldronContext.BACK - 1] = (c)->back(c);
    }
    
    private final void back(ContextEntry ctxentry) {
        if (ctxentry.pagestack.size() > 0)
            ctxentry.pagestack.pop();
    }
    
    private final XMLElement build(ContextEntry ctxentry, ToolData data)
    		throws Exception {
    	XMLElement element;
    	ToolFactory factory;
    	
    	if (data.invisible)
    		return new XMLElement("span");
    	
    	if (data.type == null)
    		throw new CauldronException(
    				"type not specified for %s.", data.name);
    	
    	factory = (data.factory == null)?
    	        getFactory(data.type.getType()) : data.factory;
    	if (factory == null)
    		return null;
    	
    	factory.execute(ctxentry, data, null);
    	element = new XMLElement(data.tag);
    	for (String name : data.attributes.keySet())
    	    element.add(name, data.attributes.get(name));
        if (data.items.size() == 0)
            element.addInner((data.text == null)? "" : data.text);
    	element.addChildren(build(ctxentry, data.items));
    	return element;
    }
    
    private final List<XMLElement> build(ContextEntry ctxentry,
    		Map<String, ToolData> items) throws Exception {
    	XMLElement xmlelement;
    	List<XMLElement> elements = new ArrayList<>();
    	
    	for (String key : items.keySet()) {
    	    xmlelement = build(ctxentry, items.get(key));
    	    if (xmlelement != null)
    	        elements.add(xmlelement);
    	}
    	
    	return elements;
    }
    
    private final void headscript(ContextEntry ctxentry, XMLElement head) {
        XMLElement script = new XMLElement("script");
        script.addInner(new String[] {
            new StringBuilder("sigma_back_listener_add('").
                append(ctxentry.pagectx.getResponseComponent()).
                append("', 'index.html', '").
                append(ctxentry.currentform).append("');").toString(),
            "history.pushState(null, null, '');" });
        head.addChild(script);
        
    }
    
    private final void home(ContextEntry ctxentry) {
        while (ctxentry.pagestack.size() > 0)
            ctxentry.pagestack.pop();
    }
    
    private final String renderer(ContextEntry ctxentry) throws Exception {
        ToolData data;
        XMLElement element;
        String[][] hitem;
        StringBuilder sb;
        Map<String, String[][]> hitems;
        XMLElement body;
        XMLElement html = new XMLElement("html");
        XMLElement head = new XMLElement("head");
        
        ctxentry.context.reset();
        if (!ctxentry.reload) {
            data = ctxentry.pagectx.instance(
                    ctxentry.pagectx.getResponseComponent());
            sb = new StringBuilder();
            for (String key : data.items.keySet())
                sb.append(build(ctxentry, data.items.get(key)).toString());
            return sb.toString();
        }
        
        hitems = ctxentry.pagectx.getHead();
        for (String key : hitems.keySet()) {
            hitem = hitems.get(key);
            element = new XMLElement(hitem[0][0]);
            if (hitem[0].length > 1)
                element.addInner(hitem[0][1]);
            for (int i = 1; i < hitem.length; i++)
                element.add(hitem[i][0], hitem[i][1]);
            head.addChild(element);
        }
    
        body = new XMLElement("body");
        data = ctxentry.pagectx.instance("view");
        for (String key : data.items.keySet())
            body.addChild(build(ctxentry, data.items.get(key)));
        
        headscript(ctxentry, head);
    
        html.head("<!DOCTYPE html>");
        html.addChild(head);
        html.addChild(body);
        
        return html.toString();
    }
}

interface TargetDetermination {
	
	public abstract String get(ContextEntry ctxentry, Handler handler);
	
}

interface PageNavigation {
    
    public abstract void execute(ContextEntry ctxentry);
    
}
