package org.quantic.cauldron.application.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.application.ContextEntry;

public class WebContextEntry extends ContextEntry {
    public HttpServletRequest req;
    public HttpServletResponse resp;
    public Exception ex;
    
    public WebContextEntry(Context context) {
    	super(context);
    }
}
