package org.quantic.cauldron.datamodel;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.quantic.cauldron.application.SharedContext;

public class DataContext {
    private Map<String, DataContextEntry> entries;
    private Map<String, Set<String>> references;
    private SharedContext sharedctx;
    
    public DataContext(SharedContext sharedctx) {
        this.sharedctx = sharedctx;
        this.entries = new HashMap<>();
        this.references = new HashMap<>();
    }
    
    public final void clear() {
        entries.clear();
    }
    
    public final DataContextEntry get(String name) {
        DataContextEntry entry = entries.get(name);
        return (entry == null)? null : entry;
    }
    
    private final String getFullName(String parent, String name) {
        return new StringBuilder(parent).append(".").append(name).toString();
    }
    
    public final Set<String> getReferences(String type) {
        return references.get(type);
    }
    
    public final DataContextEntry instance(TypeContextEntry type, String name) {
        DataContextEntry entry = entries.get(name);
        if (entry == null)
            set(name, entry = new DataContextEntry(this, type, name, null));
        return entry;
    }
    
    public final DataContextEntry instance(String type, String name) {
        String[] ctype = sharedctx.types.get(type);
        TypeContextEntry typedef = sharedctx.
                get(ctype[0]).getContext().typectx().get(ctype[1]);
        return instance(typedef, name);
    }
    
    public final Set<String> instances() {
        return entries.keySet();
    }
    
    public final void set(String name, DataContextEntry entry) {
        Set<String> reference;
        String typename;
        TypeContextEntry type;
        
        if (entry == null)
            return;
        entries.put(name, entry);
        type = entry.getType();
        reference = references.get(typename = type.getName());
        if (reference == null) {
            references.put(typename, reference = new HashSet<>());
            references.put(type.getPath(), reference);
        }
        reference.add(name);
        for (String key : entry.getItems())
            set(getFullName(name, key), entry.getItem(key));
    }
    
    public final void reset() {
        references.clear();
        entries.clear();
    }
}
