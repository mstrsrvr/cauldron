package org.quantic.cauldron.datamodel;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.quantic.cauldron.application.CauldronException;

public class DataContextEntry {
    private String name;
    private Object value;
    private TypeContextEntry type;
    private DataContext datactx;
    private Map<String, String> items;
    
    public DataContextEntry(DataContext datactx,
    		TypeContextEntry type, String name, Object value) {
    	String itemname;
    	
    	if (type == null)
    		throw new CauldronException("undefined type for %s.", name);
    	
    	this.name = name;
    	this.type = type;
    	this.value = value;
    	this.datactx = datactx;
    	items = new LinkedHashMap<>();
    	for (String key : type.getItems()) {
    		items.put(key, itemname = getFullName(key));
    		if (type.isAllocable(key))
    			datactx.instance(type.get(key), itemname);
    	}
    }
    
    public final void clear() {
        if (!type.isArray())
            throw new CauldronException(
                "clear() permitido somente para ascendente array");
        items.clear();
    }
    
    public final void copy(DataContextEntry data) {
    	for (String key : items.keySet())
    		if (data.exists(key))
    			set(key, (Object)data.getItem(key).get());
    }
    
    public final boolean exists(String name) {
    	return items.containsKey(name);
    }
    
    @SuppressWarnings("unchecked")
    public final <T> T get() {
    	return (T)value;
    }
    
    public final <T> T get(String name) {
    	DataContextEntry item = getItem(name);
    	return (item == null)? null : item.get();
    }
    
    public final boolean getbl() {
        return (value == null)? false : (boolean)value;
    }
    
    public final boolean getbl(String name) {
    	return getItem(name).getbl();
    }
    
    public final double getd(String name) {
    	Object value = get(name);
    	return (value == null)? 0 : (double)value;
    }
    
    public final float getf(String name) {
    	Object value = get(name);
    	return (value == null)? 0 : (float)value;
    }
    
    public final int geti(String name) {
    	Object value = get(name);
    	return (value == null)? 0 : (int)value;
    }
    
    private final String getFullName(String name) {
        return new StringBuilder(this.name).append(".").append(name).toString();
    }
    
    public final DataContextEntry getItem(String name) {
    	return datactx.get(items.get(name));
    }
    
    public final Set<String> getItems() {
    	return items.keySet();
    }
    
    public final long getl(String name) {
    	Object value = get(name);
    	return (value == null)? 0l : (long)value;
    }
    
    public final String getName() {
    	return name;
    }
    
    public final String getst(String name) {
    	Object value = get(name);
    	return (value == null)? null : (String)value;
    }
    
    public final TypeContextEntry getType() {
    	return type;
    }
    
    public final DataContextEntry instance() {
    	if (!type.isArray())
    		throw new CauldronException(
    			"set(index, data) permitido somente para ascendente array");
    	int index = items.size();
    	String itemname = new StringBuilder(name).
    			append(".").append(index).toString();
    	items.put(itemname, itemname);
    	return datactx.instance(type.getParent(), itemname);
    }
    
    public final DataContextEntry instance(String name) {
        DataContextEntry data = getItem(name);
        if (data != null)
            return data;
        data = datactx.instance(getType().get(name), getFullName(name));
        set(name, data);
        return data;
    }
    
    public final void remove(String item) {
        if (!type.isArray())
            throw new CauldronException(
                "remove(index) permitido somente para ascendente array");
        items.remove(item);
    }
    
    public final void set(Object value) {
        this.value = value;
    }
    
    public final void set(String name, DataContextEntry value) {
        datactx.set(items.get(name), value);
    }
    
    public final void set(String name, Object value) {
        if (type.isAllocable(name))
            getItem(name).set(value);
        else
            set(name, (DataContextEntry)value);
    }
    
    public final void setn(double factor, String unit, double exp) {
        value = new Operand(factor, unit, exp);
    }
    
    public final void setn(String name, double factor, String unit, double exp)
    {
    	getItem(name).set(new Operand(factor, unit, exp));
    }
    
    @Override
    public final String toString() {
        String value;
        int i = 0;
        StringBuilder sb = new StringBuilder(name).append("(").
                append(type.getFullName()).append(")=").
                append((this.value == null)? "null" : this.value.toString()).
                append(" {");
        for (String key : items.keySet()) {
            if (i++ > 0)
                sb.append(", ");
            sb.append(key).append("=").append(
                    ((value = get(key)) == null)? "null" : value.toString());
        }
        return sb.append("}").toString();
    }
}
