package org.quantic.cauldron.datamodel;

import java.util.HashMap;
import java.util.Map;

public class Operand {
	public double factor, exp;
	public String name, unit;
	public Map<String, Operand> operands;
	public byte operation;
	
	public Operand(double factor, String name, double exp) {
	    this();
		this.factor = factor;
		this.name = name;
		this.exp = exp;
	}
	
	public Operand() {
        operands = new HashMap<>();
	}
	
	private final boolean checkops(
	        Map<String, Operand> source, Map<String, Operand> target) {
        for (String key : source.keySet()) {
            if (!target.containsKey(key))
                return false;
            if (!source.get(key).equals(target.get(key)))
                return false;
        }
        return true;
	}
	
	@Override
	public final boolean equals(Object object) {
	    Operand operand;
	    if (object == null)
	        return false;
	    if (object == this)
	        return true;
	    if (!(object instanceof Operand))
	        return false;
	    
	    operand = (Operand)object;
	    if ((name == null && operand.name != null) ||
	            (unit == null && operand.unit != null)) {
	           if (operand.operands.size() != 1)
	               return false;
	           for (String key : operand.operands.keySet())
	               return equals(operand.operands.get(key));
	    }
	    
	    if ((factor != operand.factor) ||
	            (exp != operand.exp) || (operation != operand.operation))
	        return false;
	    
	    if (operands.size() != operand.operands.size())
	        return false;
	    
	    if (!checkops(operands, operand.operands))
	        return false;
	    
	    if (!checkops(operand.operands, operands))
	        return false;
	    
	    return true;
	}
	
	@Override
    public final String toString() {
        StringBuilder expression = new StringBuilder();
        
        if (operands.size() != 0) {
            expression.append("(");
            for (String ckey : operands.keySet())
                expression.append(operands.get(ckey).toString());
            return expression.append(")").toString();
        }
        if (name == null)
            expression.append(factor).append("E").append(exp);
        else
            expression.append(factor).append(name).append("E").append(exp);
        return expression.toString();
    }
}
