package org.quantic.cauldron.datamodel;

public enum RangeSign {
    INCLUDE, EXCLUDE
}
