package org.quantic.cauldron.datamodel;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.quantic.cauldron.application.CauldronException;

public class TypeContextEntry {
    private String name, key, path, fullname;
    private TypeContextEntry parent;
    private Map<String, TypeContextEntryDetail> items;
    private boolean autogenkey, array;
    private SharedTypeContext shtypectx;
    
    public TypeContextEntry(SharedTypeContext shtypectx,
            TypeContextEntry parent, String name, String key) {
        this.name = name;
        this.parent = parent;
        this.shtypectx = shtypectx;
        this.key = key;
        items = new LinkedHashMap<>();
        path = new StringBuilder(shtypectx.facility).append(".").
                append(getAbsoluteName(parent, name)).toString();
        fullname = new StringBuilder(shtypectx.facility).append(".").
                append(name).toString();
    }
    
    private final TypeContextEntry add(
    		TypeContextEntry type, String name, boolean allocate) {
        String absolute = getAbsoluteName(this, name);
        items.put(name, new TypeContextEntryDetail(type, allocate));
        shtypectx.types.put(absolute, type);
        return type;
    }
    
    public final TypeContextEntry add(TypeContextEntry type, String name) {
    	return add(type, name, false);
    }
    
    public final TypeContextEntry addbl(String name) {
    	return add(shtypectx.types.get("boolean"), name, true);
    }
    
    public final TypeContextEntry addd(String name) {
        return add(shtypectx.types.get("double"), name, true);
    }
    
    public final TypeContextEntry adddt(String name) {
    	return add(shtypectx.types.get("date"), name, true);
    }
    
    public final TypeContextEntry addi(String name) {
        return add(shtypectx.types.get("int"), name, true);
    }
    
    public final TypeContextEntry addl(String name) {
    	return add(shtypectx.types.get("long"), name, true);
    }
    
    public final TypeContextEntry addn(String name) {
        return add(shtypectx.types.get("numop"), name, true);
    }
    
    public final TypeContextEntry adds(TypeContextEntry type, String name) {
        return add(type, name, true);
    }
    
    public final TypeContextEntry addst(String name) {
        return add(shtypectx.types.get("string"), name, true);
    }
    
    public final TypeContextEntry addtm(String name) {
    	return add(shtypectx.types.get("time"), name, true);
    }
    
    public final void array() {
    	array = true;
    }
    
    public final void autoGenKey() {
    	if (key == null)
    		throw new CauldronException(
    				"auto-generated key for %s requires key definition.", name);
    	autogenkey = true;
    }
    
    public final void copy(TypeContextEntry type) {
    	for (String key : type.getItems())
    		add(type.get(key), key, true);
    }
    
    public final TypeContextEntry get(String name) {
    	return items.get(name).entry;
    }
    
    private final String getAbsoluteName(TypeContextEntry parent, String name) {
        StringBuilder sb;
        TypeContextEntry type;
        if (parent == null)
            return name;
        sb = new StringBuilder();
        if ((type = parent.getParent()) != null)
            sb.append(getAbsoluteName(type, parent.getName()));
        else
            sb.append(parent.getName());
        return sb.append(".").append(name).toString();
    }
    
    public final String getFullName() {
        return fullname;
    }
    
    public final Set<String> getItems() {
    	return items.keySet();
    }
    
    public final String getName() {
    	return name;
    }
    
    public final String getKey() {
    	return key;
    }
    
    public final TypeContextEntry getParent() {
    	return parent;
    }
    
    public final String getPath() {
        return path;
    }
    
    public final boolean isAllocable(String name) {
    	return items.get(name).allocate;
    }
    
    public final boolean isArray() {
    	return array;
    }
    
    public final boolean isAutoGenKey() {
    	return autogenkey;
    }
    
    public final TypeContextEntry ref(TypeContextEntry entry, String name) {
        return add(entry, name, false);
    }
}

class TypeContextEntryDetail {
	public TypeContextEntry entry;
	public boolean allocate;
	
	public TypeContextEntryDetail(TypeContextEntry entry, boolean allocate) {
		this.entry = entry;
		this.allocate = allocate;
	}
}
