package org.quantic.cauldron.dbconnector;

import org.quantic.cauldron.application.DBSession;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.datamodel.Query;
import org.quantic.cauldron.datamodel.TypeContextEntry;

public class DummySession implements DBSession {

    
    @Override
    public final void commit() { }
    
    @Override
    public final DataContextEntry get(TypeContextEntry type, Object key) {
        return null;
    }
    
    @Override
    public final void insert(DataContextEntry data) { }
    
    @Override
    public final void rollback() { }
    
    @Override
    public final void save(DataContextEntry data) { }
    
    @Override
    public final void start() { }
    
    @Override
    public final void update(Query query) { }
    
    @Override
    public final void update(DataContextEntry data) { }
    
}