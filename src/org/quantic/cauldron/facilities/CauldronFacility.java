package org.quantic.cauldron.facilities;

import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.facility.AbstractFacility;

public class CauldronFacility extends AbstractFacility {

    public CauldronFacility(Context context, String name) {
        super(context, name);
    }
    
}
