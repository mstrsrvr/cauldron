package org.quantic.cauldron.facilities.pageactions;

import org.quantic.cauldron.facility.AbstractFunction;
import org.quantic.cauldron.facility.FunctionConfig;
import org.quantic.cauldron.facility.Session;

public class Back extends AbstractFunction {

	public Back() {
		super("back");
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void config(FunctionConfig config) { }

	@Override
	protected void execute(Session session) throws Exception {
		session.context().back();
	}
	
}