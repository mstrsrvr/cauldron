package org.quantic.cauldron.facilities.pageactions;

import org.quantic.cauldron.facility.AbstractFunction;
import org.quantic.cauldron.facility.FunctionConfig;
import org.quantic.cauldron.facility.Session;

public class Home extends AbstractFunction {

	public Home() {
		super("home");
	}

	@Override
	protected void config(FunctionConfig config) { }

	@Override
	protected void execute(Session session) throws Exception {
		session.context().home();
	}

}
