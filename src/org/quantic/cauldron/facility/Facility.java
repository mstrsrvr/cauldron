package org.quantic.cauldron.facility;

import java.util.Set;

import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.datamodel.DataContextEntry;

public interface Facility {
	
	public abstract void add(Function function);
	
	public abstract <C extends Context> C getContext();
	
	public abstract Set<String> getFunctions();
	
	public abstract FunctionDetails getFunctionDetails(String function);
	
	public abstract String getName();
	
	public abstract DataContextEntry run(FunctionCall fnccall) throws Exception;
}
