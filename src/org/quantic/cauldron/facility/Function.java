package org.quantic.cauldron.facility;

import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.datamodel.DataContextEntry;

public interface Function {
    
    public abstract FunctionDetails getMetaData();
    
    public abstract String getName();
    
    public abstract DataContextEntry run(Context context, FunctionCall args)
            throws Exception;
}

