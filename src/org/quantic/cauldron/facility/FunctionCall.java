package org.quantic.cauldron.facility;

import org.quantic.cauldron.application.Context;

public class FunctionCall {
    public String function;
    public Object input, workid, argret;
    public Exception retex;
    public Message message;
    public Context context;
    
    public FunctionCall(Context context, String function, Object input) {
        this.function = function;
        this.input = input;
        this.context = context;
    }
    
    @Override
    public final String toString() {
        return new StringBuilder(function).append("(").
                append((input == null)? "null" : input.toString()).
                append(")").toString();
    }
}
