package org.quantic.cauldron.facility;

import java.util.HashMap;
import java.util.Map;

public class FunctionDetails {
    private Map<String, String> input;
    private Map<String, Integer> inputcounter;
    public String output;
    public boolean template, splitoutput;
    
    public FunctionDetails() {
        input = new HashMap<>();
        inputcounter = new HashMap<>();
    }
    
    public final void add(String name, String type) {
        input.put(name, type);
        if (!inputcounter.containsKey(type))
            inputcounter.put(type, 1);
        else
            inputcounter.put(type, inputcounter.get(type) + 1);
    }
    
    public final Map<String, String> getInputs() {
        return input;
    }
    
    @Override
    public final String toString() {
        StringBuilder sb = new StringBuilder("input: ").
                append(inputcounter.toString());
        
        if (output != null)
            sb.append(" output: ").append(output.toString());
        
        return sb.toString();
    }
}
