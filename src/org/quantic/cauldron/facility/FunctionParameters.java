package org.quantic.cauldron.facility;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.datamodel.DataContext;
import org.quantic.cauldron.datamodel.DataContextEntry;

public class FunctionParameters {
    public boolean splitoutput;
    public String outputtype, outputname;
    private Map<String, MetaInput> inputs;
    
    public FunctionParameters() {
        inputs = new HashMap<>();
    }
    
    public final void add(String input, String object) {
        inputs.put(input, new MetaInput(object));
    }
    
    public final void add(String input) {
        inputs.put(input, new MetaInput());
    }
    
    public final void compile(Context context) {
        MetaInput input;
        DataContext datactx = context.datactx();
        
        for (String key : inputs.keySet())
            if (!(input = inputs.get(key)).function)
                input.value = datactx.get(input.name);
    }
    
    public final DataContextEntry get(String input) {
        return inputs.get(input).value;
    }
    
    public final String getName(String input) {
        return inputs.get(input).name;
    }
    
    public final boolean isFunction(String input) {
        return inputs.get(input).function;
    }
    
    public final Set<String> keySet() {
        return inputs.keySet();
    }
    
    public final void set(String input, DataContextEntry value) {
        inputs.get(input).value = value;
    }
    
    @Override
    public final String toString() {
        MetaInput mi;
        String sep = null;
        StringBuilder sb = new StringBuilder();
        for (String key : inputs.keySet())
            sb.append((sep == null)? sep = "" : ", ").append(
                    (mi = inputs.get(key)).function? "function()" : mi.name);
        return sb.toString();
    }
}

class MetaInput {
    String name;
    public DataContextEntry value;
    public boolean function;
    
    public MetaInput(String name) {
        this.name = name;
    }
    
    public MetaInput() {
        function = true;
    }
}
