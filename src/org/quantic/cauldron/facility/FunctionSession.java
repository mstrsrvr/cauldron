package org.quantic.cauldron.facility;

import org.quantic.cauldron.application.Context;

public interface FunctionSession {
    
    public abstract <C extends Context> C context();
    
    public abstract void error(Exception e);
    
    public abstract Exception getError();
    
    public abstract FunctionParameters input();
    
}
