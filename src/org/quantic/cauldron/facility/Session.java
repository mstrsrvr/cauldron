package org.quantic.cauldron.facility;

import org.quantic.cauldron.application.CauldronException;
import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.trace.ProgramSignalException;
import org.quantic.cauldron.trace.SignalData;

public class Session {
    private FunctionSession session;
    private Message message;
    private Context userctx;
    private DataContextEntry output;
    
    public Session(Context userctx, FunctionSession session) {
        this.session = session;
        this.userctx = userctx;
    }

    @SuppressWarnings("unchecked")
    public final <C extends Context> C context() {
        return (C)session.context();
    }
    
    public final void error(String text, Object... args) {
    	message = new Message(Message.TYPE.ERROR, text, args);
    	throw new CauldronException(text, args);
    }
    
    public final <T> T get(String name) {
        return geto(name).get();
    }
    
    public final Message getMessage() {
    	return message;
    }
    
    public final DataContextEntry geto(String name) {
        FunctionParameters taskdata = session.input();
        return taskdata.get(name);
    }
    
    public final DataContextEntry output() {
        FunctionParameters taskdata = session.input();
        if (output != null)
            return output;
        return output = userctx.datactx().instance(
                taskdata.outputtype, taskdata.outputname);
    }
    
    public final void set(DataContextEntry entry) {
        output = entry;
    }
    
    public final void signal(SignalData data) throws Exception {
        throw new ProgramSignalException(data);
    }
    
    public final void status(String text, Object... args) {
    	message = new Message(Message.TYPE.STATUS, text, args);
    }
    
    public final Context userctx() {
        return userctx;
    }
    
    public final void warning(String text, Object... args) {
    	message = new Message(Message.TYPE.WARNING, text, args);
    }
}

