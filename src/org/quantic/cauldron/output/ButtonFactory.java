package org.quantic.cauldron.output;

import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.pagectx.ToolData;

public class ButtonFactory extends AbstractToolFactory {

	public ButtonFactory() {
		super("button");
	}
	
	@Override
	protected void generate(ContextEntry config, ToolData data, String prefix) {
		data.attributes.put("type", (data.submit)? "submit" : "button");
		data.attributes.put("name", data.name);
		data.attributes.put("data-sigma-form", config.currentform);
		data.attributes.put("data-sigma-action", data.name);
		data.attributes.put(
				"data-sigma-updtarea", config.pagectx.getResponseComponent());
		data.text = toString(
		        config, (data.text == null)? data.name : data.text);
	}
}
