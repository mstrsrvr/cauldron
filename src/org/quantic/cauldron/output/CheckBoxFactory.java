package org.quantic.cauldron.output;

import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.pagectx.PageSpecItem.TYPES;
import org.quantic.cauldron.pagectx.ToolData;

public class CheckBoxFactory extends AbstractToolFactory {

	public CheckBoxFactory() {
		super("div");
	}
	
	@Override
	protected void generate(ContextEntry config, ToolData data, String prefix) {
		ToolData element;
		DataContextEntry entry;
		
		data.attributes.put("id", data.name.concat("_cnt"));
		data.style = data.styles.get("cb_external");
		
		element = data.instance(TYPES.STANDARD_CONTAINER, data.name);
		element.tag = "input";
		element.attributes.put("type", "checkbox");
		element.style = data.styles.get("checkbox");
		entry = config.context.datactx().get(data.name);
		if ((entry != null) && entry.getbl())
		    element.attributes.put("checked", "checked");
		config.pagectx.addInput(data.name);
		if (data.text == null)
			return;
		
		element = data.instance(TYPES.TEXT, data.name.concat("_label"));
		element.tag = "label";
		element.style = data.styles.get("cb_label");
	}
}
