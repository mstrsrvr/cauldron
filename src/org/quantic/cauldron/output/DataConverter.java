package org.quantic.cauldron.output;

import org.quantic.cauldron.application.ContextEntry;

public interface DataConverter {
	
	public abstract Object run(ContextEntry ctxentry, String value)
			throws Exception;
}
