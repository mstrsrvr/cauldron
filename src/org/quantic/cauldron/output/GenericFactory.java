package org.quantic.cauldron.output;

import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.pagectx.ToolData;

public class GenericFactory extends AbstractToolFactory {

	public GenericFactory(String tag) {
		super(tag);
	}
	
	@Override
	protected void generate(ContextEntry config, ToolData data, String prefix) {
		
	}
}
