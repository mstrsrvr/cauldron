package org.quantic.cauldron.output;

import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.pagectx.ToolData;

public class LinkFactory extends AbstractToolFactory {

	public LinkFactory() {
		super("a");
	}
	
	@Override
	protected void generate(ContextEntry config, ToolData data, String prefix)
	        throws Exception {
        data.attributes.put("data-sigma-action",
                (data.action == null)? data.name : data.action);
        data.attributes.put(
                "data-sigma-updtarea", config.pagectx.getResponseComponent());
        if (data.absolute)
            data.attributes.put("href", data.source);
        data.text = (data.model != null)?
                toString(config, data) : toString(config, data.text);
	}
}
