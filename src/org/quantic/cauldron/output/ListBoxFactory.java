package org.quantic.cauldron.output;

import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.datamodel.DataContext;
import org.quantic.cauldron.pagectx.ToolData;
import org.quantic.cauldron.pagectx.PageSpecItem.TYPES;

public class ListBoxFactory extends AbstractToolFactory {

    public ListBoxFactory() {
    	super("select");
    }
    
    @Override
    protected void generate(ContextEntry config, ToolData data, String prefix) {
        ToolData item;
        Object value;
        DataContext datactx;
        
        if (data.model != null) {
            datactx = config.context.datactx();
            for (String object : datactx.getReferences(data.model)) {
                value = datactx.get(object).get();
                data.current = (value == null)? null : value.toString();
                break;
            }
        }
        
        data.items.clear();
        config.pagectx.addInput(data.name);
        for (String key : data.values.keySet()) {
            item = data.instance(TYPES.TEXT, new StringBuilder(data.name).
                    append(".").append(key).toString());
            item.tag = "option";
            value = data.values.get(key);
            item.text = (value == null)? "" : value.toString();
            if (data.current == null)
                data.current = item.name;
            item.attributes.put("value", key);
            if (!data.current.equals(key))
                item.attributes.remove("selected");
            else
                item.attributes.put("selected", "selected");
        }
    }
}
