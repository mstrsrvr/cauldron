package org.quantic.cauldron.output;

import java.util.HashMap;
import java.util.Map;

import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.pagectx.PageSpecItem.TYPES;
import org.quantic.cauldron.pagectx.ToolData;

public class TabbedPaneFactory extends AbstractToolFactory {
    
	public TabbedPaneFactory() {
		super("div");
	}
	
    @Override
    protected final void generate(
    		ContextEntry ctxentry, ToolData widget, String prefix) {
    	ToolData button, buttonbar_item, panesarea, tabbedpane, pane;
    	ToolData buttonbar = widget.instance(TYPES.NODE_LIST,
    			new StringBuilder(widget.name).append("_buttons").toString());
    	Map<String, String> labels = new HashMap<>();
    	
    	buttonbar.style = widget.styles.get("button_bar");
    	buttonbar.attributes.put("role", "tablist");
    	for (String key : widget.items.keySet()) {
    		if (key.equals(buttonbar.name))
    			continue;
    		if (widget.current == null)
    			widget.current = key;
    		tabbedpane = widget.items.get(key);
    		if (tabbedpane.type.getType() != TYPES.TABBED_PANE_ITEM)
    		    continue;
    		buttonbar_item = buttonbar.instance(TYPES.NODE_LIST_ITEM,
				new StringBuilder(key).append("_button_bar_item").toString());
    		buttonbar_item.style = widget.styles.get("button_bar_item");
    		
    		button = buttonbar_item.instance(TYPES.LINK,
    		        new StringBuilder(key).append("_button").toString());
    		button.text = key;
    		button.attributes.put("role", "tab");
    		button.attributes.put("data-toggle", "pill");
    		button.absolute = true;
    		button.source = new StringBuilder("#").append(key).toString();
    		button.action = key;
    		if (widget.current.equals(key)) {
    			button.style = widget.styles.get("active_button");
    			button.attributes.put("aria-selected", "true");
    		} else {
    			button.style = widget.styles.get("deactive_button");
    			button.attributes.put("aria-selected", "false");
    		}
    		labels.put(key, button.name);
    		
    		ctxentry.pagectx.action(key, "page-actions", "tab_change");
    	}
    	
    	panesarea = widget.instance(TYPES.STANDARD_CONTAINER,
    			new StringBuilder(widget.name).append("_panes").toString());
    	panesarea.style = widget.styles.get("panes_area");
    	for (String key : widget.items.keySet()) {
    		if (key.equals(buttonbar.name) || key.equals(panesarea.name))
    			continue;
    		tabbedpane = widget.items.get(key);
    		tabbedpane.style = widget.styles.get(key.equals(widget.current)?
    				"active_pane" : "deactive_pane");
    		tabbedpane.attributes.put("role", "tabpanel");
    		tabbedpane.attributes.put("aria-labelledby", labels.get(key));
    		
    		pane = panesarea.instance(TYPES.STANDARD_CONTAINER, key);
    		pane.attributes.putAll(tabbedpane.attributes);
    		pane.style = tabbedpane.style;
    		pane.items.putAll(tabbedpane.items);
    	}
    }
}
