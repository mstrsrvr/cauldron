package org.quantic.cauldron.output;

import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.datamodel.DataContext;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.datamodel.TypeContextEntry;
import org.quantic.cauldron.pagectx.PageSpecItem.TYPES;
import org.quantic.cauldron.pagectx.ToolData;

public class TableFactory extends AbstractToolFactory {
    
    public TableFactory() {
    	super("table");
    }
    
    private final ToolData cell(
            String facility, String model, String typename, ToolData widget,
    		TYPES type, String name, Object value, String tag) {
        String cellname = new StringBuilder(widget.name).append(".").
        		append(name).toString();
        ToolData cell = widget.instance(type, cellname);
        cell.tag = tag;
        cell.style = widget.styles.get("cell");
        cell.text = (value == null)? null : value.toString();
        cell.facility = facility;
        if (typename == null)
            return cell;
        cell.model = new StringBuilder(model).append(".").append(typename).
                toString();
        return cell;
    }
    
    @Override
    protected void generate(ContextEntry ctxentry,
            ToolData widget, String prefix) throws Exception {
        String modelname;
        ToolData item, thead, tbody;
        DataContextEntry data;
        TypeContextEntry type = getType(ctxentry, widget);
        DataContext datactx = ctxentry.context.datactx();
        
        if ((data = datactx.get(widget.name)) == null)
        	data = datactx.instance(type, widget.name);
        
        widget.items.clear();
        thead = line(
        		widget,
        		"thead",
        		null,
        		new StringBuilder(widget.name).append("_thead").toString());
        
        item = line(
        		thead,
        		"tr",
        		null,
        		new StringBuilder(widget.name).
        			append(".").append("head").toString());
        
        type = type.getParent();
        for (String key : type.getItems())
        	cell(null, null, null, item, TYPES.TEXT, key, key, "th");
        
        tbody = line(
        		widget,
        		"tbody",
        		null,
        		new StringBuilder(widget.name).append("_tbody").toString());
        
        modelname = type.getName();
        for (String key : data.getItems()) {
        	item = line(tbody, "tr", "item", key);
        	for (String ikey : type.getItems())
        		cell(widget.facility, modelname, ikey, item,
        			ikey.equals(widget.mark)? TYPES.CHECK_BOX : TYPES.TEXT,
        			ikey, data.getItem(key).get(ikey), "td");
        }
    }
    
    private final ToolData line(
    		ToolData widget, String tag, String style, String name) {
    	ToolData item = widget.instance(TYPES.STANDARD_CONTAINER, name);
    	item.tag = tag;
    	item.style = (style == null)? null : widget.styles.get(style);
    	
    	return item;
    }
}
