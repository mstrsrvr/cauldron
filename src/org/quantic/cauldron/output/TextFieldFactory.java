package org.quantic.cauldron.output;

import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.pagectx.ToolData;

public class TextFieldFactory extends AbstractToolFactory {

	public TextFieldFactory() {
		super("input");
	}
	
	@Override
	protected void generate(ContextEntry config, ToolData data, String prefix)
	        throws Exception {
		data.attributes.put("name", data.name);
		data.attributes.put("type", data.secret? "password" : "text");
		data.attributes.put("value", toString(config, data));
		config.pagectx.addInput(data.name);
	}
}
