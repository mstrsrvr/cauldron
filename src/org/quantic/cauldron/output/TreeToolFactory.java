package org.quantic.cauldron.output;

import org.quantic.cauldron.application.ContextEntry;
import org.quantic.cauldron.datamodel.DataContext;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.datamodel.TypeContextEntry;
import org.quantic.cauldron.pagectx.PageSpecItem.TYPES;
import org.quantic.cauldron.pagectx.ToolData;

public class TreeToolFactory extends AbstractToolFactory {
    
    public TreeToolFactory() {
    	super("table");
    }
    
    private final void build(ContextEntry ctxentry, ToolData widget,
            DataContextEntry data) throws Exception {
        ToolData child;
        
        if (data.get() != null) {
            child = widget.instance(
                    TYPES.NODE_LIST_ITEM, widget.name.concat("_item"));
            child.text = toString(ctxentry, data);
        }
        
        for (String key : data.getItems()) {
            child = widget.instance(TYPES.NODE_LIST, key);
            build(ctxentry, child, data.getItem(key));
        }
    }
    
    @Override
    protected final void generate(ContextEntry ctxentry,
            ToolData widget, String prefix) throws Exception {
        ToolData child;
        DataContextEntry data;
        TypeContextEntry type = getType(ctxentry, widget);
        DataContext datactx = ctxentry.context.datactx();
        
        if ((data = datactx.get(widget.name)) == null)
        	data = datactx.instance(type, widget.name);
        
        child = widget.instance(TYPES.NODE_LIST, widget.name.concat("_tree"));
        build(ctxentry, child, data);
    }
}
