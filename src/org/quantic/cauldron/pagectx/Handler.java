package org.quantic.cauldron.pagectx;

public class Handler {
    public static final byte DIRECT = 0;
    public static final byte TARGET = 1;
    public String facility, function;
    public byte type;
    
    public Handler(String facility, String function, byte type) {
    	this.facility = facility;
    	this.function = function;
    	this.type = type;
    }
}
