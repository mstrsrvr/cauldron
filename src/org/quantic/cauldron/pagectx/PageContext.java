package org.quantic.cauldron.pagectx;

import java.util.Map;
import java.util.Set;

public interface PageContext {
	
    public abstract void action(String name, String facility, String function);
    
    public abstract void add(ToolData data);
    
    public abstract void addInput(String name);
    
    public abstract void addSpecAlias(String alias, String spec);
    
    public abstract ToolData get(String name);
    
    public abstract PageContext getChild(String name);
    
    public abstract Set<String> getChildren();
    
    public abstract PageConfig getConfig();
    
    public abstract Handler getHandler(String name);
    
    public abstract Map<String, String[][]> getHead();
    
    public abstract Set<String> getInputs();
    
    public abstract Messages getMessages(String locale);
    
    public abstract PageContext getParent();
    
    public abstract String getResponseComponent();
    
    public abstract PageSpec getSpec();
    
    public abstract ToolData instance(String tooldata);
    
    public abstract ToolData instance(PageSpecItem specitem);
    
    public abstract boolean isSubPage(String name);
    
    public abstract void put(String name, PageContext page);
    
    public abstract void set(CauldronPage cpage);

    public abstract void set(PageContext page);
    
    public abstract void setResponseComponent(String name);
    
    public abstract void setTitle(String text, Object... args);
    
    public abstract void target(String name, String target);
}
