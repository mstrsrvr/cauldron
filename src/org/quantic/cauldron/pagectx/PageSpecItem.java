package org.quantic.cauldron.pagectx;

public class PageSpecItem {

	public enum TYPES {
        BUTTON("button"),
        CHECK_BOX("checkbox"),
        DATA_FORM("dataform"),
        DUMMY("dummy"),
        EXPAND_BAR("expandbar"),
        FORM("form"),
        FILE_UPLOAD("fileupload"),
        FRAME("frame"),
        LINK("link"),
        LISTBOX("listbox"),
        MESSAGE("message"),
        NONE("none"),
        NODE_LIST("nodelist"),
        NODE_LIST_ITEM("nodelistitem"),
        PAGE_CONTROL("navcontrol"),
        PRINT_AREA("printarea"),
        RADIO_BUTTON("radiobutton"),
        RADIO_GROUP("radiogroup"),
        REPORT_TOOL("reporttool"),
        STANDARD_CONTAINER("standardcontainer"),
        TABBED_PANE("tabbedpane"),
        TABBED_PANE_ITEM("tabbedpaneitem"),
        TABLE_TOOL("tabletool"),
        TEXT("text"),
        TEXT_EDITOR("texteditor"),
        TEXT_FIELD("textfield"),
        TILES("tiles"),
        TREE_TOOL("treetool"),
        PARAMETER("parameter"),
        VIEW("view"),
        VIRTUAL_CONTROL("virtualcontrol");

        private String name;

        TYPES(String name) {
            this.name = name;
        }
        
        @Override
        public final String toString() {
            return name;
        }
    };
    
    private String parent, name;
    private TYPES type;
    
    public PageSpecItem(String parent, TYPES type, String name) {
        this.parent = parent;
        this.type = type;
        this.name = name;
    }
    
    public final String getName() {
        return name;
    }
    
    public final String getParent() {
        return parent;
    }
    
    public final TYPES getType() {
        return type;
    }
}
