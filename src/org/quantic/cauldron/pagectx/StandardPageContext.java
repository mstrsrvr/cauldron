package org.quantic.cauldron.pagectx;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class StandardPageContext implements PageContext {
    private Map<String, String[][]> hitems;
    private Map<String, ToolData> entries;
    private Map<String, ChildPage> children;
    private Map<String, String> specalias;
    private Set<String> inputs;
    private Map<String, Handler> handlers;
    private Map<String, Messages> translations;
    private PageContext parent;
    private String respcomponent;
    private CauldronPage cpage;
    
    public StandardPageContext() {
    	hitems = new LinkedHashMap<>();
    	entries = new LinkedHashMap<>();
    	children = new LinkedHashMap<>();
    	specalias = new HashMap<>();
    	inputs = new HashSet<>();
    	handlers = new HashMap<>();
    	translations = new HashMap<>();
    }
    
    @Override
    public final void action(String name, String facility, String function) {
    	handlers.put(name, new Handler(facility, function, Handler.DIRECT));
    }
    
    @Override
    public final void add(ToolData data) {
        entries.put(data.name, data);
    }
    
    @Override
    public final void addInput(String name) {
    	inputs.add(name);
    }
    
    @Override
    public final void addSpecAlias(String alias, String spec) {
    	specalias.put(alias, spec);
    }
    
    @Override
    public final ToolData get(String name) {
        return getToolData(name);
    }
    
    @Override
    public final PageContext getChild(String name) {
        ChildPage cpage = children.get(name);
        return (cpage == null)? null : cpage.page;
    }
    
    @Override
    public final Set<String> getChildren() {
    	return children.keySet();
    }
    
    @Override
    public final PageConfig getConfig() {
    	return cpage.config;
    }
    
    @Override
    public final Handler getHandler(String name) {
    	return handlers.get(name);
    }
    
    @Override
    public final Map<String, String[][]> getHead() {
    	return hitems;
    }
    
    @Override
    public final Set<String> getInputs() {
    	return inputs;
    }
    
    @Override
    public final Messages getMessages(String locale) {
        Messages messages = translations.get(locale);
        if (messages == null)
            translations.put(locale, messages = new Messages());
        return messages;
    }
    
    @Override
    public final PageContext getParent() {
        return parent;
    }
    
    @Override
    public final String getResponseComponent() {
    	return respcomponent;
    }
    
    @Override
    public final PageSpec getSpec() {
    	return cpage.spec;
    }
    
    private final ToolData getToolData(String name) {
    	ToolData tooldata;
    	return ((tooldata = entries.get(name)) != null)?
    			tooldata : entries.get(specalias.get(name));
    }
    
    @Override
    public final ToolData instance(String tooldata) {
        ToolData data = getToolData(tooldata);
        if (data == null) {
            if (parent != null)
                return parent.instance(tooldata);
            add(data = new ToolData(null, tooldata));
        }
        return data;
    }
    
    @Override
    public final ToolData instance(PageSpecItem specitem) {
    	ToolData pdata;
    	String name = specitem.getName();
        ToolData data = getToolData(name);
        
        if (data != null)
        	return data;
        if (parent != null)
            return parent.instance(name);
        
        add(data = new ToolData(null, name));
    	data.type = specitem;
    	data.parent = specitem.getParent();
    	pdata = instance(data.parent);
    	pdata.items.put(data.name, data);
    	return data;
    }
    
    @Override
    public final boolean isSubPage(String name) {
        return children.get(name).subpage;
    }
    
    @Override
    public final void put(String name, PageContext page) {
        page.set(this);
        children.put(name, new ChildPage(page, false));
    }
    
    @Override
    public void set(CauldronPage cpage) {
        this.cpage = cpage;
    }
    
    @Override
    public void set(PageContext page) {
        parent = page;
    }
    
    @Override
    public final void setResponseComponent(String name) {
    	respcomponent = name;
    }
    
    @Override
    public final void setTitle(String text, Object... args) {
    	hitems.put("title", new String[][] {
    		{"title", String.format(text, args)} });
    }
    
    @Override
    public final void target(String name, String target) {
        handlers.put(name, new Handler(null, target, Handler.TARGET));
    }
}

class ChildPage {
    public PageContext page;
    public boolean subpage;
    
    public ChildPage(PageContext page, boolean subpage) {
        this.page = page;
        this.subpage = subpage;
    }
}
