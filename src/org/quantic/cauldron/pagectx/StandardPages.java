package org.quantic.cauldron.pagectx;

import org.quantic.cauldron.application.ApplicationContext;
import org.quantic.cauldron.pagectx.base.BasePageContext;
import org.quantic.cauldron.pagectx.panel.PanelPageContext;

public class StandardPages {
	private ApplicationContext appctx;
	
	public StandardPages(ApplicationContext appctx) {
	    this.appctx = appctx;
	}
	
	public final PageContext base(String name) {
		return new BasePageContext(appctx, name);
	}
	
	public final PageContext instance(String name) {
		return new BasePageContext(appctx, new PanelPageContext(), name);
	}
}
