package org.quantic.cauldron.pagectx.base;

import org.quantic.cauldron.application.ApplicationContext;
import org.quantic.cauldron.pagectx.CauldronPage;
import org.quantic.cauldron.pagectx.CauldronPageType;
import org.quantic.cauldron.pagectx.PageContext;
import org.quantic.cauldron.pagectx.StandardPageContext;

public class BasePageContext extends StandardPageContext {
    private PageContext custom;
    
    public BasePageContext(ApplicationContext appctx, String name) {
        init(appctx, this, name);
    }
    
    public BasePageContext(
            ApplicationContext appctx, PageContext pagectx, String name) {
        init(appctx, pagectx, name);
        put("custom", pagectx);
    }
    
    private final void init(
            ApplicationContext appctx, PageContext next, String name) {
        CauldronPage cpage = new CauldronPage(
                new BasePageSpec(), new BasePageConfig());
        cpage.type = CauldronPageType.BASE;
        appctx.pages.get(name).pagectx = this;
        super.set(cpage);
        custom = new StandardPageContext();
        next.put("custom", custom);
    }
    
    @Override
    public final void set(CauldronPage page) {
        custom.set(page);
    }
}
