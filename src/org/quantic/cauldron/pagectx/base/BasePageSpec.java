package org.quantic.cauldron.pagectx.base;

import org.quantic.cauldron.pagectx.AbstractPageSpec;

public class BasePageSpec extends AbstractPageSpec {

	@Override
	protected void execute() {
		form("main");
		container("main", "fullcontent");
		message("fullcontent", "message");
		spec("fullcontent", "custom");
	}
	
}

