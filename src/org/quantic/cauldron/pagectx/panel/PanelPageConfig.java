package org.quantic.cauldron.pagectx.panel;

import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.pagectx.AbstractPageConfig;
import org.quantic.cauldron.pagectx.PageContext;
import org.quantic.cauldron.pagectx.ToolData;

public class PanelPageConfig extends AbstractPageConfig<Context> {

	@Override
	protected void execute(PageContext pagectx, Context context) {
        ToolData data;
        
        data = getTool("navbar");
        data.style = "navbar fixed-top navbar-expand-lg navbar-dark bg-primary";
        data.tag = "nav";
        
        data = getTool("navbarcontent");
        data.style = "container";
        
        data = getTool("titlebar");
        data.attributes.put("style", "padding-top:4rem;text-align:center;");
        data.tag = "h1";
        
        data = getTool("logo");
        data.style = "navbar-brand mr-5";
        data.source = "login.html";
        data.absolute = true;
        
        data = getTool("logoimg");
        data.attributes.put("src", "./imgs/qs-branco.png");
        data.attributes.put("alt", "logo-quantic-software");
        data.attributes.put("width", "120");
        data.tag = "img";
        
        data = getTool("content");
        data.style = "container";
        
        config("custom");
	}
	
}
