package org.quantic.cauldron.pagectx.panel;

import org.quantic.cauldron.pagectx.CauldronPage;
import org.quantic.cauldron.pagectx.CauldronPageType;
import org.quantic.cauldron.pagectx.StandardPageContext;

public class PanelPageContext extends StandardPageContext {

	public PanelPageContext() {
		CauldronPage cpage = new CauldronPage(
		        new PanelPageSpec(), new PanelPageConfig());
		cpage.type = CauldronPageType.PANEL;
		super.set(cpage);
	}
}
