package org.quantic.cauldron.pagectx.panel;

import org.quantic.cauldron.pagectx.AbstractPageSpec;

public class PanelPageSpec extends AbstractPageSpec {

	@Override
	protected void execute() {
        container("fullcontent", "navbar");
        container("navbar", "navbarcontent");
        link("navbarcontent", "logo");
        container("logo", "logoimg");
        container("fullcontent", "content");
        container("fullcontent", "status");
        text("content", "titlebar");
        spec("content", "custom");
	}
	
}

