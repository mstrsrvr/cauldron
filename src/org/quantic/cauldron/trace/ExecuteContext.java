package org.quantic.cauldron.trace;

import org.quantic.cauldron.application.Application;
import org.quantic.cauldron.application.Context;

public class ExecuteContext {
    public String faclog, fnclog, facility, extfac;
    public ContextPool ctxpool;
    public int sessionid, pinlevel;
    public SignalData signal;
    public Application application;
    public Context context;
}
