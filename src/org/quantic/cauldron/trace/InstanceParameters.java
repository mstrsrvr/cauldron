package org.quantic.cauldron.trace;

import org.quantic.cauldron.datamodel.DataContextEntry;

public class InstanceParameters {
    public String iname, type;
    public DataContextEntry object;
}
