package org.quantic.cauldron.trace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.quantic.cauldron.datamodel.DataContext;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.facility.AbstractFunction;
import org.quantic.cauldron.facility.Facility;
import org.quantic.cauldron.facility.FunctionConfig;
import org.quantic.cauldron.facility.FunctionDetails;
import org.quantic.cauldron.facility.Session;

public class MakeSpec {
    public ExecuteContext execctx;
    public WorkItem reference;
    public int clevel, programid;
    public Map<String, List<String[]>> templates;
    public boolean levellimit;
    public String levelcheckpoint;
    private Set<String> branches;
    
    public MakeSpec() {
        branches = new HashSet<>();
        templates = new HashMap<>();
    }
    
    public final void addBranch(String branch) {
        branches.add(branch);
    }

    public final void clearBranches(byte type) {
        branches.clear();
    }
    
    public final WorkItem clone(WorkItem reference) {
        WorkItem workitem = new WorkItem();
        workitem.pitem = reference.pitem;
        workitem.level = reference.level;
        workitem.ctxdata.addAll(reference.ctxdata);
        return workitem;
    }
    
    public final ProgramItem clone(
        WorkItemData widata, Map<String, String[]> input, ProgramItem item) {
        String name, type;
        DataContextEntry value;
        ProgramItem nitem = new ProgramItem(
                item.facility, item.function, item.reference, item.id);
        DataContext datactx = execctx.context.datactx();
        
        nitem.source = item.source;
        nitem.oconn = item.oconn;
        nitem.taskdata.outputname = nitem.oconn.name;
        nitem.taskdata.outputtype = nitem.oconn.parameter;
        
        for (String ikey : item.taskdata.keySet()) {
            nitem.tinputs.put(ikey, item.tinputs.get(ikey));
            if (item.taskdata.isFunction(ikey)) {
                nitem.taskdata.add(ikey);
                continue;
            }
            
            if (widata == null) {
                nitem.taskdata.add(ikey, item.taskdata.getName(ikey));
                continue;
            }
            
            type = datactx.get(item.taskdata.getName(ikey)).getType().getName();
            for (String fkey : input.keySet()) {
                if (widata.pinputs.contains(fkey))
                    continue;
                value = datactx.get(name = input.get(fkey)[0]);
                if (!value.getType().getName().equals(type))
                    continue;
                nitem.tinputs.put(ikey, name);
                nitem.taskdata.add(ikey, name);
                widata.pinputs.add(fkey);
                break;
            }
        }
        
        return nitem;
    }
    
    public final ContextPool cloneContextPool() {
        ContextPool dctxpool = new ContextPool();
        for (String okey : execctx.context.datactx().instances())
            copy(dctxpool, okey);
        return dctxpool;
    }
    
    public final boolean containsBranch(String branch) {
        return branches.contains(branch);
    }

    private final String copy(ContextPool ctxpool, String from) {
        InstanceParameters params;
        DataContextEntry ofrom = execctx.context.datactx().get(from);
        params = new InstanceParameters();
        params.iname = from;
        params.object = ofrom;
        params.type = ofrom.getType().getName();
        return ctxpool.instance(params);
    }
    
    public final void createExtensions() throws Exception {
        List<Object[]> postinstall;

        postinstall = new ArrayList<>();
        for (String facility : execctx.context.getFacilities())
            if (!facility.equals(execctx.extfac))
                insertFunctionsConnections(postinstall, facility);
        
        for (Object[] objects : postinstall)
            new DummyFunction(
                    execctx.context.getFacility((String)objects[0]),
                    (String)objects[1],
                    (FunctionDetails)objects[2]);
    }
    
    public static final String getParameterName(
            String facility, String function, String key) {
        return new StringBuilder(facility).append(".").
                append(function).append(":").append(key).toString();
    }
    
    public final boolean hasAnyBranch() {
        return branches.size() > 0;
    }
    
    private final void insertFunctionsConnections(
            List<Object[]> postinstall, String facname) {
        FunctionDetails fnccfg;
        Map<String, String> inputs;
        String signature;
        Object[] install;
        Facility facility = execctx.context.getFacility(facname);
        
        for (String fnckey : facility.getFunctions()) {
            if ((fnccfg = facility.getFunctionDetails(fnckey)) == null)
                continue;
            if (fnccfg.template)
                continue;
            inputs = fnccfg.getInputs();
            for (String name : inputs.keySet()) {
                execctx.ctxpool.connect(
                        facname, fnckey, "input", name, inputs.get(name));
                continue;
            }

            execctx.ctxpool.connect(
                    facname, fnckey, "output", "output", fnccfg.output);
            
            if (!isTemplateRegistered(signature = fnccfg.toString()))
                if ((install = registerTemplate(facname, fnccfg)) != null)
                    postinstall.add(install);
            registerFunction(signature, facname, fnckey);
        }
    }
    
    public final boolean isTemplateRegistered(String signature) {
        return templates.containsKey(signature);
    }
    
    public final void registerFunction(
            String signature, String facility, String function) {
        templates.get(signature).add(new String[] {facility, function});
    }
    
    public final Object[] registerTemplate(
            String facility, FunctionDetails fnccfg) {
        String type;
        Map<String, String> inputs = fnccfg.getInputs();
        String function = new StringBuilder("_template_").
                append(templates.size()).toString();
        FunctionDetails tmpfnccfg = new FunctionDetails();
        
        for (String key : inputs.keySet()) {
            execctx.ctxpool.connect(execctx.extfac,
                    function, "input", key, type = inputs.get(key));
            tmpfnccfg.add(key, type);
        }
        execctx.ctxpool.connect(execctx.extfac,
                function, "template-output", "output", fnccfg.output);
        tmpfnccfg.output = fnccfg.output;
        tmpfnccfg.template = true;
        templates.put(tmpfnccfg.toString(), new ArrayList<>());
        if (facility.equals(execctx.extfac))
            return new Object[] {facility, function, tmpfnccfg};
        new DummyFunction(execctx.context.
                getFacility(execctx.extfac), function, tmpfnccfg);
        return null;
    }
    
    public final List<ProgramItem[]> shuffleProgramItems(
            ProgramItem[] items, int itemnr) {
        List<ProgramItem[]> programs = new ArrayList<>();

        shuffleProgramItems(programs, items, itemnr);
        
        return programs;
    }
    
    private final void shuffleProgramItems(
            List<ProgramItem[]> programs, ProgramItem[] items, int itemnr) {
        ProgramItem[] ditems;
        int ditemnr;
        String type, name, output;
        Set<String> tinputs;
        Map<String, String> inputs;
        DataContextEntry dobj;
        DataContext datactx;
        ProgramItem sitem = items[itemnr];
        FunctionDetails sfnccfg = execctx.context.getFacility(sitem.facility).
                getFunctionDetails(sitem.function);
        
        if (!sfnccfg.template) {
            if (items.length == 1)
                programs.add(items);
            else
                shuffleProgramItems(programs, items, itemnr + 1);
            return;
        }
        datactx = execctx.context.datactx();
        tinputs = new HashSet<>();
        for (String[] function : templates.get(sfnccfg.toString())) {
            ditems = new ProgramItem[items.length];
            ditems[itemnr] = new ProgramItem(
                    function[0], function[1], sitem.reference, sitem.id);
            ditems[itemnr].oconn = execctx.ctxpool.
                    connectionInstance(output = getParameterName(
                            function[0], function[1], "output"));
            ditems[itemnr].tinputs = sitem.tinputs;
            ditems[itemnr].source = sitem.source;
            inputs = execctx.context.getFacility(function[0]).
                    getFunctionDetails(function[1]).getInputs();
            for (String key : inputs.keySet()) {
                type = inputs.get(key);
                for (String sitemkey : sitem.taskdata.keySet()) {
                    if (sitem.taskdata.isFunction(sitemkey)) {
                        ditems[itemnr].taskdata.add(key);
                        continue;
                    }
                    
                    dobj = datactx.get(sitem.taskdata.getName(sitemkey));
                    if (!dobj.getType().getPath().equals(type))
                        continue;
                    if (tinputs.contains(name = dobj.getName()))
                        continue;
                    ditems[itemnr].taskdata.add(key, name);
                    tinputs.add(name);
                    break;
                }
            }
            tinputs.clear();
            for (int i = 0; i < items.length; i++)
                if (i != itemnr)
                    ditems[i] = items[i];
            ditems[sitem.reference] = clone(null,null,ditems[sitem.reference]);
            ditems[sitem.reference].tinputs.put(sitem.source, output);
            if ((ditemnr = itemnr + 1) < items.length) {
                shuffleProgramItems(programs, ditems, ditemnr);
                continue;
            }
            programs.add(ditems);
        }
    }
}

class DummyFunction extends AbstractFunction {
    private FunctionConfig config;
    private FunctionDetails details;
    
    public DummyFunction(
            Facility facility, String name, FunctionDetails details) {
        super(name);
        this.details = details;
        config(config);
        facility.add(this);
        config.get().template = details.template;
    }

    @Override
    protected void config(FunctionConfig config) {
        Map<String, String> inputs;
        
        if (details == null) {
            this.config = config;
            return;
        }
        
        inputs = details.getInputs();
        for (String key : inputs.keySet())
            config.input(inputs.get(key), key);
        config.output(details.output);
    }

    @Override
    protected void execute(Session session) throws Exception { }
    
}
