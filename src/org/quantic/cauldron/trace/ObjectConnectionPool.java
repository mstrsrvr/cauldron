package org.quantic.cauldron.trace;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ObjectConnectionPool {
    private Map<String, ObjectConnection> connections;
    public Map<String, Set<String>> aliases;
    
    public ObjectConnectionPool() {
        connections = new HashMap<>();
        aliases = new HashMap<>();
    }
    
    public final void compile(String key) {
        ObjectConnection connection;
        Set<String> alias;
        
        connection = connections.get(key);
        if (connection.parameter == null) {
            alias = aliases.get(key);
            if (alias == null)
                aliases.put(key, alias = new HashSet<>());
            connection.parent = key;
        } else {
            alias = aliases.get(connection.parameter);
            if (alias == null)
                aliases.put(connection.parameter, alias = new HashSet<>());
            connection.parent = connection.parameter;
        }
        alias.add(key);
    }
    
    public final void compile() {
        for (String key : connections.keySet())
            compile(key);
    }
    
    public final Set<ObjectConnection> get(String name) {
        Set<ObjectConnection> selection;
        Set<String> alias = aliases.get(name);
        
        if (alias == null)
            return null;
        selection = new HashSet<>();
        for (String dname : alias)
            selection.add(connections.get(dname));
        return selection;
    }
    
    public final ObjectConnection instance(String name) {
        ObjectConnection data = connections.get(name);
        if (data != null)
            return data;
        connections.put(name, data = new ObjectConnection());
        data.name = name;
        return data;
    }
    
    public final Set<String> instances() {
        return connections.keySet();
    }
}
