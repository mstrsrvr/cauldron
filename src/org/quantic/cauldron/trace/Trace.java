package org.quantic.cauldron.trace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.application.TestContext;
import org.quantic.cauldron.datamodel.DataContext;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.datamodel.TypeContextEntry;
import org.quantic.cauldron.facility.FunctionDetails;

public class Trace {
    private static final int FUNCTION_NAME = 1;
    private static final int OBJECT_NAME = 0;
    
    public static final TestProgram assemblyProgram(
            MakeSpec spec, WorkItemData widata) {
        CauldronProgram program = new CauldronProgram(spec.programid++);
        int i = 0;
        
        program.items = new ProgramItem[widata.program.size()];
        program.inputs = widata.pinputs;
        program.oconn = widata.oconn;
        for (ProgramItem item : widata.program)
            program.items[i++] = item;
        return new TestProgram(program, spec);
    }
    
    private static final String[] convertInput(Map<String, String[]> inputs) {
        int i = 0;
        if (inputs == null)
            return new String[0];
        String[] input = new String[inputs.size()];
        for (String key : inputs.keySet())
            input[i++] = inputs.get(key)[0];
        return input;
    }
    
    public static final WorkItemData copyShuffledItems(
            MakeSpec spec, ProgramItem[] items, Map<String, String[]> input) {
        WorkItemData widata = new WorkItemData();
        for (ProgramItem item : items)
            widata.program.add(spec.clone(widata, input, item));
        return widata;
    }
    
    public static final List<CauldronProgram> execute(
            ExecuteContext execctx) throws Exception {
        TestProgram test;
        List<TestProgram> tests;
        List<CauldronProgram> validated = new ArrayList<>();
        int resultlimit = 0;
        MakeSpec spec = prepare(execctx);

        while (true) {
            try {
                test = generate(spec);
            } catch (TerminateException e) {
                break;
            }
            if (test == null)
                continue;
            tests = shuffle(spec, test);
            if (tests == null) {
                if (!test(spec, test))
                    continue;
                validated.add(test.program);
                if (spec.execctx.ctxpool.resultlimit == 0)
                    continue;
                resultlimit++;
                if (resultlimit == spec.execctx.ctxpool.resultlimit)
                    return validated;
                continue;
            }
            
            for (TestProgram _test : tests) {
                if (!test(spec, _test))
                    continue;
                validated.add(_test.program);
                if (spec.execctx.ctxpool.resultlimit == 0)
                    continue;
                resultlimit++;
                if (resultlimit == spec.execctx.ctxpool.resultlimit)
                    return validated;
            }
        }
        
        return validated;
    }
    
    private static final TestProgram generate(MakeSpec spec)
            throws Exception {
        TestProgram program;
        WorkItemData widata = new WorkItemData();

        widata.noaction = true;
        if (spec.levelcheckpoint == null)
            spec.clevel = (spec.clevel == -1)? 0 :
                (spec.levellimit)? spec.clevel + 1 : spec.clevel;
        spec.levellimit = false;
        spec.programid = spec.clevel * 10000;
        widata.fnccmp = false;
        program = specworker(spec, widata);
        if (program != null)
            return program;
        widata.fnccmp = true;
        program = specworker(spec, widata);
        if (widata.noaction && !spec.levellimit)
            throw new TerminateException();
        return program;
    }
    
    private static final StringBuilder getBranch(List<ProgramItem> program) {
        StringBuilder branch = null;
        for (ProgramItem item : program) {
            if (branch == null)
                branch = new StringBuilder("[");
            else
                branch.append(".[");
            branch.append(item.toString()).append("]");
        }
        return branch;
    }
    
    private static final String getBranch(
            ObjectConnection oconn, String[] input, List<ProgramItem> program) {
        StringBuilder branch;
        
        if (oconn == null)
            return null;
        if ((branch = getBranch(program)) == null)
            branch = new StringBuilder("[");
        else
            branch.append(".[");
        return branch.append(
                getCurrentExpression(oconn, input)).append("]").toString();
    }
    
    private static final List<Map<String, String[]>> getCompatibleInputs(
            MakeSpec spec, WorkItemData widata) {
        Map<String, String> finputs = getInputs(spec, widata.oconn);
        
        if (finputs.size() == 0)
            return null;

        return getCompatibleInputs(
                spec.execctx.context,
                spec.execctx.ctxpool,
                widata.oconn,
                widata.reference,
                finputs);
    }
    
    private static final List<Map<String, String[]>> getCompatibleInputs(
            Context context, ContextPool ctxpool, ObjectConnection oconn,
            WorkItem workitem, Map<String, String> finputs) {
        Map<String, String[]> sinput;
        List<Map<String, String[]>> sinputs;
        Set<ObjectConnection> inputs;
        String finame, fsource;
        Set<String> dup;
        int lines, c, fisize = 0;
        List<String[]> largs = null;
        DataContext datactx = context.datactx();
        
        for (String fikey : finputs.keySet()) {
            fisize++;
            finame = MakeSpec.getParameterName(
                    oconn.facility, oconn.function, fikey);
            inputs = getCompatibleSources(ctxpool, finame);
            if (inputs == null)
                continue;
            for (ObjectConnection iconn : inputs) {
                if (iconn.name.equals(finame))
                    continue;
                fsource = null;
                if ((datactx.get(iconn.name) == null) &&
                        ((fsource = getFunctionSource(iconn)) == null))
                    continue;
                if ((fsource != null) &&
                        !iconn.direction.equals("template-output"))
                    continue;
                if ((fsource == null) && (workitem != null) &&
                        !workitem.ctxdata.contains(iconn.name))
                    continue;
                if (largs == null)
                    largs = new ArrayList<>();
                largs.add(new String[] {fikey, iconn.name, fsource});
            }
        }
        
        if ((largs == null) || largs.size() == 0)
            return null;
        lines = (int)Math.pow(2, largs.size());
        sinputs = null;
        dup = new HashSet<>();
        for (int i = 0; i < lines; i++) {
            c = 0;
            sinput = null;
            dup.clear();
            for (String[] args : largs) {
                c = (c == 0)? 1 : c << 1;
                if ((i & c) == 0)
                    continue;
                if ((sinput != null) && sinput.containsKey(args[0])) {
                    sinput = null;
                    break;
                }
                if (sinput == null)
                    sinput = new HashMap<>();
                if (isDuplicated(dup, args)) {
                    sinput.clear();
                    sinput = null;
                    break;
                }
                dup.add(args[1]);
                sinput.put(args[0], new String[] {args[1], args[2]});
            }
            if ((sinput == null) || (sinput.size() != fisize))
                continue;
            if (sinputs == null)
                sinputs = new ArrayList<>();
            sinputs.add(sinput);
        }
        
        return sinputs;
    }
    
    private static final Set<ObjectConnection> getCompatibleSources(
            ContextPool ctxpool, String target) {
        ObjectConnection oconn = getTypeByAbsoluteName(ctxpool, target);
        return (oconn == null)?
                null : getSourcesByType(ctxpool, oconn.parameter);
    }
    
    private static final String getCurrentExpression(
            ObjectConnection oconn, String[] input) {
        StringBuilder branch = new StringBuilder();
        
        if (oconn.facility == null)
            branch.append(oconn.name);
        else
            branch.append(oconn.facility).append(".").append(oconn.function).
                    append("(");
        for (int i = 0; i < input.length; i++) {
            if (i > 0)
                branch.append(",");
            branch.append(input[i]);
        }

        if (oconn.facility != null)
            branch.append(")");
        return branch.toString();
    }

    private static final String getExpression(
            TaskDataParameters tdparam, List<ProgramItem> program) {
        String input[] = convertInput(tdparam.input);
        return getBranch(tdparam.oconn, input, program);
    }
    
    private static final String getFunctionSource(ObjectConnection conn) {
        return (conn.direction == null)?
                null : (conn.direction.equals("input")? null : conn.function);
    }
    
    private static final Map<String, String> getInputs(
            MakeSpec spec, ObjectConnection oconn) {
        FunctionDetails config = spec.execctx.context.getFacility(
                oconn.facility).getFunctionDetails(oconn.function);
        return config.getInputs();
    }
    
    private static final Set<ObjectConnection> getSourcesByType(
            ContextPool ctxpool, String type) {
        return ctxpool.getConnections(type);
    }
    
    private static final ObjectConnection getTypeByAbsoluteName(
            ContextPool ctxpool, String name) {
        return ctxpool.connectionInstance(name);
    }
  
    public static final boolean isDataObjectOnly(Map<String, String[]> input) {
        for (String ikey : input.keySet())
            if (input.get(ikey)[1] != null)
                return false;
        return true;
    }
    
    private static final boolean isDuplicated(Set<String> dup, String[] args) {
        return (args[2] != null)? false : dup.contains(args[1]);
    }
    
    private static final boolean isEarlierBranch(
            MakeSpec spec, String expression) {
        return spec.containsBranch(expression);
    }
    
//    private static final boolean isFactorySkippable(
//            MakeSpec spec, ProgramItem[] items) {
//        List<String[]> skippable;
//        for (String fackey : spec.context.getFacilities()) {
//            skippable = spec.context.getFacility(fackey).;
//            for (String[] sample : skippable) {
//                if (sample.length > items.length)
//                    continue;
//                if (isSkipCompatible(fackey, items, sample))
//                    return true;
//            }
//        }
//        return false;
//    }
//    
//    private static final boolean isSkipCompatible(
//            String facility, ProgramItem[] items, String[] sample) {
//        int pos;
//        boolean compatible = false;
//        
//        for (int j = items.length - 1; j >= 1; j--) {
//            compatible = true;
//            for (int i = 0; i < sample.length; i++)
//                if (!items[pos = j - i].facility.equals(facility) ||
//                        !items[pos].function.equals(sample[i])) {
//                    compatible = false;
//                    break;
//                }
//            if (compatible)
//                return true;
//        }
//        return compatible;
//    }
    
    private static final MakeSpec prepare(
            ExecuteContext execctx) throws Exception {
        MakeSpec spec = new MakeSpec();
        TestContext testctx = new TestContext();
        
        spec.execctx = execctx;
        spec.clevel = spec.execctx.pinlevel;
        spec.reference = new WorkItem();
        testctx.datactx = spec.execctx.context.datactx();
        testctx.signal = execctx.signal;
        execctx.ctxpool.target.definputs(testctx);
        for (String key : testctx.datactx.instances()) {
            spec.execctx.ctxpool.instance(testctx.datactx.get(key));
            spec.reference.ctxdata.add(key);
        }
        spec.execctx.context.setLogTarget(
                spec.execctx.faclog, spec.execctx.fnclog);
        spec.createExtensions();
        return spec;
    }
    
    private static final void prepareTaskData(TaskDataParameters tdparam)
            throws Exception {
        String[] args;
        ObjectConnection oconn;
        WorkItem workitem;
        WorkItemData widata;
        int originallevel, currentlevel, nro = 0, nrinputs = 0;
        Exception signal = null;
        
        if (tdparam.reference.level > tdparam.spec.clevel) {
            tdparam.spec.levellimit = true;
            return;
        }
        
        /*
         * calcularemos o consumo de objetos de contexto, para saber se há
         * objetos suficientes para processsamento dessa função.
         */
        for (String ikey : tdparam.input.keySet()) {
            args = tdparam.input.get(ikey);
            oconn = tdparam.spec.execctx.ctxpool.cpool.instance(
                    args[OBJECT_NAME]);
            if (oconn.facility == null)
                nro++;
        }
        
        /*
         * se o número de parâmetros da expressão for maior que o número de
         * objetos no pool de dados, essa expressão não pode ser satisfeita.
         */
        if (nro > tdparam.reference.ctxdata.size())
            return;
        
        originallevel = tdparam.widata.program.size();
        workitem = tdparam.spec.clone(tdparam.reference);
        workitem.level++;
        workitem.reference = tdparam.reference;
        workitem.outputname = MakeSpec.getParameterName(
                tdparam.oconn.facility, tdparam.oconn.function, "output");
        tdparam.widata.program.push(workitem.pitem = new ProgramItem(
                tdparam.oconn.facility, tdparam.oconn.function,
            (tdparam.reference.pitem == null)? -1 : tdparam.reference.pitem.id,
            tdparam.widata.program.size()));
        workitem.pitem.oconn = tdparam.oconn;
        workitem.pitem.source = tdparam.widata.source;
        workitem.pitem.taskdata.outputtype = workitem.pitem.oconn.parameter;
        workitem.pitem.taskdata.outputname = workitem.pitem.oconn.name;
        
        for (String ikey : tdparam.input.keySet()) {
            args = tdparam.input.get(ikey);
            workitem.pitem.tinputs.put(ikey, args[OBJECT_NAME]);
            if (args[FUNCTION_NAME] != null)
                continue;
            workitem.pitem.taskdata.add(ikey, args[OBJECT_NAME]);
            workitem.remove(args[OBJECT_NAME]);
            tdparam.widata.pinputs.add(args[OBJECT_NAME]);
            nrinputs++;
        }
        
        for (String ikey : tdparam.input.keySet()) {
            args = tdparam.input.get(ikey);
            if (args[FUNCTION_NAME] == null)
                continue;
            
            workitem.pitem.taskdata.add(ikey);
            oconn = tdparam.spec.execctx.ctxpool.cpool.instance(
                    args[OBJECT_NAME]);
            try {
                widata = tdparam.widata.child();
                widata.reference = workitem;
                widata.oconn = oconn;
                widata.source = ikey;
                workitemexec(tdparam.spec, widata);
                break;
            } catch (AssemblySuccessException e) {
                signal = e;
                nrinputs++;
            }
        }
        
        if (nrinputs < tdparam.input.size()) {
            currentlevel = tdparam.widata.program.size();
            for (int i = originallevel; i < currentlevel; i++)
                tdparam.widata.program.pop();
            return;
        }

        workitem.reference.ctxdata = workitem.ctxdata;
        throw (signal == null)?
            new AssemblySuccessException(tdparam.widata.expression) : signal;
    }
    
    private static final void saveBranch(MakeSpec spec, String branch) {
        spec.addBranch(branch);
    }
    
    private static final void saveShuffledItems(
            MakeSpec spec, TestProgram test, Map<String, String[]> input) {
        WorkItemData widata =
                copyShuffledItems(spec, test.program.items, input);
        String name = getBranch(widata.program).toString();
        saveBranch(spec, name);
    }
    
    private static final List<TestProgram> shuffle(
            MakeSpec spec, TestProgram test) {
        BuildPrograms build;
        ObjectConnection oconn, ioconn;
        Map<String, String> finputs;
        List<Map<String, String[]>> inputs;
        String name;
        ContextPool ctxpool;
        
        if (test.program.items.length == 0)
            return null;
        
        finputs = new HashMap<>();
        ctxpool = spec.cloneContextPool();
        for (String key : test.program.inputs) {
            ioconn = ctxpool.connectionInstance(key);
            name = ctxpool.connect(
                    "alpha", "test_one", "input", key, ioconn.parent);
            finputs.put(key, name);
        }

        oconn = new ObjectConnection();
        oconn.facility = "alpha";
        oconn.function = "test_one";
        inputs = getCompatibleInputs(
                spec.execctx.context, ctxpool, oconn, null, finputs);
        build = new BuildPrograms();
        build.oconn = test.program.oconn;
        build.spec = spec;
        if (inputs == null) {
            saveShuffledItems(spec, test, null);
            build.pitems = spec.shuffleProgramItems(test.program.items, 0);
            return build.run();
        }
        
        for (Map<String, String[]> input : inputs)
            saveShuffledItems(spec, test, input);
        
        build.pitems = spec.shuffleProgramItems(test.program.items, 0);
        return build.run(inputs);
    }
    
    private static final void span(MakeSpec spec, WorkItemData widata,
            List<Map<String, String[]>> inputs) throws Exception {
        TaskDataParameters tdparam = new TaskDataParameters(
                widata.reference, widata.oconn);
        
        /*
         * processaremos primeiramente parâmetros apenas com objetos de dados.
         */
        tdparam.widata = widata;
        tdparam.spec = spec;
        for (Map<String, String[]> input : inputs) {
            if (!isDataObjectOnly(input))
                continue;
            tdparam.input = input;
            spannext(tdparam);
        }
        
        /*
         * processaremos agora qualquer parâmetro com retorno de função.
         */
        for (Map<String, String[]> input : inputs) {
            if (isDataObjectOnly(input))
                continue;
            tdparam.input = input;
            spannext(tdparam);
        }
    }
    
    private static final void spannext(TaskDataParameters tdparam)
            throws Exception {
        tdparam.widata.expression = getExpression(
                tdparam, tdparam.widata.program);
        if (tdparam.spec.levelcheckpoint == null) {
            if (!isEarlierBranch(tdparam.spec, tdparam.widata.expression))
                prepareTaskData(tdparam);
            return;
        }
        
        if (!tdparam.spec.levelcheckpoint.startsWith(tdparam.widata.expression))
            return;
        if (isEarlierBranch(tdparam.spec, tdparam.widata.expression)) {
            if (tdparam.spec.levelcheckpoint.equals(tdparam.widata.expression))
                tdparam.spec.levelcheckpoint = null;
            return;
        }
        prepareTaskData(tdparam);
    }
    
    private static final TestProgram specworker(
            MakeSpec spec, WorkItemData widata) throws Exception {
        TestProgram program;
        Set<ObjectConnection> oconns;
        boolean specwrkrnoproc;
        TypeContextEntry type;
        
        type = spec.execctx.context.typectx().
        		get(spec.execctx.ctxpool.target.context().returns);
        oconns = spec.execctx.ctxpool.
                getConnections(type.getPath());
        specwrkrnoproc = true;
        for (ObjectConnection oconn : oconns) {
            if ((oconn.direction != null) &&
                    !oconn.direction.equals("output"))
                continue;

            if ((oconn.facility != null) != widata.fnccmp)
                continue;

            specwrkrnoproc = false;
            widata.levellimit = 0;
            widata.oconn = oconn;
            spec.execctx.context.log(
                    "** compatible output %s found.", oconn.name);
            program = workitembuilder(spec, widata);
            if (program != null)
                return program;
        }
        
        if (specwrkrnoproc && widata.fnccmp)
            throw new TerminateException();
        return null;
    }
    
    private static final boolean test(MakeSpec spec, TestProgram test)
            throws Exception {
        Context context = spec.execctx.application.context();
        if (spec.execctx.signal != null)
            spec.execctx.signal.get(context);
        return test(spec, context, test);
    }
    
    private static final boolean test(
            MakeSpec spec, Context context, TestProgram test) throws Exception {
        TestContext testctx;
        DataContextEntry dovalue;
        CauldronExecutionData execdata = new CauldronExecutionData();
        
        try {
            dovalue = test.program.execute(context, execdata);
        } catch (Exception e) {
            return false;
        }
        
        if ((dovalue == null) && (execdata.signal == null))
            throw new Exception("invalid data processed");
        
        testctx = new TestContext();
        testctx.data = dovalue;
        testctx.target = spec.execctx.ctxpool.target;
        testctx.signal = execdata.signal;
        testctx.datactx = context.datactx();
        
        if (testctx.signal == null)
            return spec.execctx.ctxpool.target.test(testctx);
        if (spec.execctx.ctxpool.target.sigtest(testctx)) {
            testctx.signal.get(context);
            return test(spec, context, test);
        }
        return false;
    }
    
    private static final TestProgram workitembuilder(
            MakeSpec spec, WorkItemData widata) throws Exception {
        try {
            widata.reference = spec.clone(spec.reference);
            widata.source = null;
            widata.program.clear();
            if (widata.oconn.facility == null)
                workiteminput(spec, widata);
            else
                workitemexec(spec, widata);
            spec.levelcheckpoint = null;
        } catch (AssemblySuccessException eas) {
            if (widata.fnccmp)
                spec.levelcheckpoint = eas.branch;
            widata.noaction = false;
            saveBranch(spec, eas.branch);
            return assemblyProgram(spec, widata);
        }

        return null;
    }
    
    private static final void workitemexec(MakeSpec spec, WorkItemData widata)
            throws Exception {
        String fname;
        List<Map<String, String[]>> inputs;
        ProgramItem item;
        
        fname = String.format("%s.%s()",
                widata.oconn.facility, widata.oconn.function);
        spec.execctx.context.log("%s is a target compatible function.", fname);
        inputs = getCompatibleInputs(spec, widata);
        if (inputs == null) {
            if (getInputs(spec, widata.oconn).size() > 0)
                return;
            widata.expression = getCurrentExpression(
                    widata.oconn, new String[0]);
            if (isEarlierBranch(spec, widata.expression))
                return;
            widata.program.push(item = new ProgramItem(
                    widata.oconn.facility,
                    widata.oconn.function,
                    (widata.reference.pitem == null)?
                            -1 : widata.reference.pitem.id,
                    widata.program.size()));
            item.oconn = widata.oconn;
            item.source = widata.source;
            throw new AssemblySuccessException(widata.expression);
        }

        span(spec, widata, inputs);
    }
    
    private static final void workiteminput(MakeSpec spec, WorkItemData widata)
            throws Exception {
        widata.expression = getExpression(
            new TaskDataParameters(widata.reference, widata.oconn),
                widata.program);
        if (isEarlierBranch(spec, widata.expression))
            return;
        if (widata.reference.level > spec.clevel)
            return;
        throw new AssemblySuccessException(widata.expression);
    }
}

class TerminateException extends Exception {
    private static final long serialVersionUID = -7672817764073950224L;
    
}

class TestProgram {
    public CauldronProgram program;
    public MakeSpec spec;
    
    public TestProgram(CauldronProgram program, MakeSpec spec) {
        this.program = program;
        this.spec = spec;
    }
}

class BuildPrograms {
    public List<ProgramItem[]> pitems;
    public MakeSpec spec;
    public ObjectConnection oconn;
    
    public final List<TestProgram> run() {
        List<TestProgram> programs = new ArrayList<>();
        run(programs, null);
        return programs;
    }
    
    private final void run(
            List<TestProgram> programs, Map<String, String[]> input) {
        WorkItemData widata;
        for (ProgramItem[] items : pitems) {
//          if (isFactorySkippable(spec, items))
//          continue;
            widata = Trace.copyShuffledItems(spec, items, input);
            widata.oconn = oconn;
            programs.add(Trace.assemblyProgram(spec, widata));
        }
    }
    
    public final List<TestProgram> run(List<Map<String, String[]>> inputs) {
        List<TestProgram> programs = new ArrayList<>();
        for (Map<String, String[]> input : inputs)
            if (Trace.isDataObjectOnly(input))
                run(programs, input);
        return programs;
    }
}
