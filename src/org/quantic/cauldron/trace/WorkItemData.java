package org.quantic.cauldron.trace;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class WorkItemData {
    public int levellimit;
    public WorkItem reference;
    public ObjectConnection oconn;
    public String source, expression;
    public Stack<ProgramItem> program;
    public Set<String> pinputs;
    public boolean fnccmp, noaction;
    
    public WorkItemData() {
        program = new Stack<>();
        pinputs = new HashSet<>();
    }
    
    public final WorkItemData child() {
        WorkItemData widata = new WorkItemData();
        widata.pinputs = pinputs;
        widata.program = program;
        return widata;
    }
}
